#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 18:40:20 2018

@author: rezwan
"""

import numpy as np
import pandas as pd
df_Clinical = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Shared AmaderGram Data 2018-DataSheet - Clinical data.csv")

### Raed Clean data
df_0850 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/df_0850_gl_new.csv")
df_0940 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/df_0940_gl_new.csv")
df_1070 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/df_1070_gl_new.csv")
df_NO = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/df_NO_gl_new.csv")

## del the glucose label
del df_0850["Glucose (mmd/L)"]
del df_0940["Glucose (mmd/L)"]
del df_1070["Glucose (mmd/L)"]
del df_NO["Glucose (mmd/L)"]

del df_0850["Age"]
del df_0940["Age"]
del df_1070["Age"]
del df_NO["Age"]

del df_0850["Sex"]
del df_0940["Sex"]
del df_1070["Sex"]
del df_NO["Sex"]

print(df_0850.shape)
print(df_0940.shape)
print(df_1070.shape)
print(df_NO.shape)

## Select column
col_list = ['ID', 'Age', 'Sex', 'Systolic_peak(x)', 'Diastolic_peak(y)',
       'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)',
       'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x',
       'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)',
       'Dicrotic_notch_time(t3)',
       'Time_between_systolic_and_diastolic_peaks(∆T)',
       'Time_between_half_systolic_peak_points(w)',
       'Inflection_point_area_ratio(A2/A1)',
       'Systolic_peak_rising_slope(t1/x)',
       'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi',
       't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2',
       '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi',
       'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi',
       '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)',
       'Fundamental_component_magnitude(|sbase|)',
       '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)',
       '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)',
       'Stress-induced_vascular_response_index(sVRI)',  'BPM']
print(len(col_list))

## Merge with Clinical Dataset
df_0850_spo2 = pd.merge(df_0850, df_Clinical, on='ID', how='outer')
df_0940_spo2 = pd.merge(df_0940, df_Clinical, on='ID', how='outer')
df_1070_spo2 = pd.merge(df_1070, df_Clinical, on='ID', how='outer')
df_NO_spo2 = pd.merge(df_NO, df_Clinical, on='ID', how='outer')


## Add new label value : SPO2
df_0850_spo2_pre = df_0850_spo2[col_list]
df_0940_spo2_pre = df_0940_spo2[col_list]
df_1070_spo2_pre = df_1070_spo2[col_list]
df_NO_spo2_pre = df_NO_spo2[col_list]

### Select subjects
df_0850_spo2_pre = df_0850_spo2_pre.iloc[0:df_0850.shape[0], ]
df_0940_spo2_pre = df_0940_spo2_pre.iloc[0:df_0940.shape[0], ]
df_1070_spo2_pre = df_1070_spo2_pre.iloc[0:df_1070.shape[0], ]
df_NO_spo2_pre = df_NO_spo2_pre.iloc[0:df_NO.shape[0], ]

### Replace Sex with Numeric
dict = {'Sex':{'Male':1, 'Female':0}}      # label = column name
df_0850_spo2_pre.replace(dict,inplace = True)
df_0940_spo2_pre.replace(dict,inplace = True)
df_1070_spo2_pre.replace(dict,inplace = True)
df_NO_spo2_pre.replace(dict,inplace = True)
 
## Generate CSV value
df_0850_spo2_pre.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/df_0850_BPM_pre.csv")
df_0940_spo2_pre.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/df_0940_BPM_pre.csv")
df_1070_spo2_pre.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/df_1070_BPM_pre.csv")
df_NO_spo2_pre.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/df_NO_BPM_pre.csv")
