#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 11:11:30 2018

@author: rezwan
"""

import random
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.model_selection import StratifiedKFold, KFold
import numpy as np
from sklearn.metrics import f1_score, r2_score

#SEED = 2018
#random.seed(SEED)
#np.random.seed(SEED)

##=============================================================================

#### read the .csv files
df_0850_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/df_0850_gl_new.csv")
df_0850_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/df_0850_hemo_new.csv")
df_0850_spo2 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/df_0850_spo2_pre.csv")

df_0940_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/df_0940_gl_new.csv")
df_0940_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/df_0940_hemo_new.csv")
df_0940_spo2 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/df_0940_spo2_pre.csv")

df_1070_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/df_1070_gl_new.csv")
df_1070_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/df_1070_hemo_new.csv")
df_1070_spo2 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/df_1070_spo2_pre.csv")

df_NO_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/df_NO_gl_new.csv")
df_NO_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/df_NO_hemo_new.csv")
df_NO_spo2 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/df_NO_spo2_pre.csv")


##### remove ID from the datasets
del df_0850_gl["ID"]
del df_0850_hm["ID"]
del df_0850_spo2["ID"]

del df_0940_gl["ID"]
del df_0940_hm["ID"]
del df_0940_spo2["ID"]

del df_1070_gl["ID"]
del df_1070_hm["ID"]
del df_1070_spo2["ID"]

del df_NO_gl["ID"]
del df_NO_hm["ID"]
del df_NO_spo2["ID"]

##================================ Calculte variance of dataset 
#print(df_0850_gl.var())
#print(df_0850_hm.var())
#
#print(df_NO_gl.var())
#print(df_NO_hm.var())
##============================================================


#### import user define function
from learning_model import *
## Filter
from feature_selection_ga_Eu import *
from fitness_function_Eu import *


def __func(df):
    df = df.as_matrix()
    df = df.astype(float)
    
#    y = df[:,[48]] ## label
    ### ================== StandardScaler
    scaler = StandardScaler()
    df = scaler.fit_transform(df)
    
    df_means = scaler.mean_
    df_stds = scaler.scale_
    ### ==================
    
    ### split Input feature and Labels
    y = df[:,[48]] ## label
#    ## Get real values
#    y = (y * df_stds[48]) * df_means[48]
#    print(y)
    #    print(y.dtype)
    X = np.delete(df, 48, axis=1)  ## input feature 
#    print(X)
    #    print(X.dtpye)
    #    
    ## split the dataset with train and test set
    ##========== If you want to take first 80% values into train set then follow (1) otherwise follow (2) 
    ############ random selection
    #####(1) 
#    train_size = int(0.8 * X.shape[0])
#    X_train, X_test, y_train, y_test = X[0:train_size], X[train_size:], y[0:train_size], y[train_size:]
#        
    ####(2)
#    from sklearn.model_selection import train_test_split
#    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    
    ### Now, You are ready for applying GA on your datset for feature selection.
    
    #### Seed
#    seed = 42
#    random.seed(seed)
#    
    ### Now run
    fsga = Feature_Selection_GA(X,y)
    pop, best_ind = fsga.generate(200, 150) ## population size and Generation = 10,50
    pp = fsga.plot_feature_set_score() ## Generation
    
    ### Set it on Machine learning Model
    get_best_ind = []
    for i in range(len(best_ind)):
        if best_ind[i] == 1:
            get_best_ind.append(i)
            
    print(len(get_best_ind))

    X_selct = X[:, get_best_ind]
    print(X_selct.shape)
    
    ###### Define the Model
    model = SVR(kernel='rbf', degree=3, C=1.0)
    ##=====================
    
    ############### Apply 10-fold Cross validation
    n_splits = 10
    cv_set = np.repeat(-1.,X_selct.shape[0])
    skf = KFold(n_splits = n_splits ,shuffle=True, random_state=42)
    for train_index,test_index in skf.split(X_selct, y):
        x_train,x_test = X_selct[train_index],X_selct[test_index]
        y_train,y_test = y[train_index],y[test_index]
        if x_train.shape[0] != y_train.shape[0]:
            raise Exception()
        model.fit(x_train,y_train)
        predicted_y = model.predict(x_test)
        cv_set[test_index] = predicted_y
     
    print(r2_score(y,cv_set))

    
    
    
#    from sklearn.model_selection import train_test_split
#    X_train, X_test, y_train, y_test = train_test_split(X_selct, y, test_size=0.2, random_state=42)
#   
##    print(X_train.dtype)
##    print(y_train.dtype)
#    svr = SVR()
#    svr.fit(X_train, y_train)
#    
#    pred_y = svr.predict(X_test)
#    
#    scr = r2_score(y_test, pred_y)
#    print(scr)
#    
if __name__ == '__main__':
    print("Here you can pass the dataset.")
    __func(df_NO_spo2)
    
""" ## 5 Folds:
  Min -0.18045158269963943
  Max 0.857111397673675
  Avg 0.5776815772268834
  Std 0.2558649004680451
-- Only the fittest survives --
Best individual is [0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 
0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 1, 0, 0, 1], (0.857111397673675,)


## 10-fold
  Min -0.143087232538814
  Max 0.8898369839101162
  Avg 0.6063314893642153
  Std 0.2484966163409287
-- Only the fittest survives --
Best individual is [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0,
0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0,
 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1], (0.8898369839101162,)
"""