#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 19:27:39 2018

@author: rezwan
"""

"""
This code from the book named: "Deep Learning with Keras"
"""

######### Import necessaries libraries.
from keras.layers import Input
from keras.layers.core import Dense
from keras.models import Model
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, Dropout, BatchNormalization, Activation, Dropout
from keras import optimizers
from sklearn.metrics import r2_score
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline


#### read the .csv files
df_0850_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/df_0850_gl_new.csv")
df_0850_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/df_0850_hemo_new.csv")

df_0940_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/df_0940_gl_new.csv")
df_0940_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/df_0940_hemo_new.csv")

df_1070_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/df_1070_gl_new.csv")
df_1070_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/df_1070_hemo_new.csv")

df_NO_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/df_NO_gl_new.csv")
df_NO_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/df_NO_hemo_new.csv")

##### remove ID from the datasets
del df_0850_gl["ID"]
del df_0850_hm["ID"]

del df_0940_gl["ID"]
del df_0940_hm["ID"]

del df_1070_gl["ID"]
del df_1070_hm["ID"]

del df_NO_gl["ID"]
del df_NO_hm["ID"]


def plot_metrics(loss, val_loss):
    fig, (ax1) = plt.subplots(1, 1, sharex='col', figsize=(5,4))
    ax1.plot(loss, label='Train loss')
    ax1.plot(val_loss, label='Validation loss')
    ax1.legend(loc='best')
    ax1.set_title('Loss')
    plt.xlabel('Epochs')
    plt.show()


def DNN_Model(X_train, y_train):
    ######################  Make a model of Two layer Dense network
    readings = Input(shape=(X_train.shape[1], )) # X_train.shape[1] = 48
    x = Dense(25, activation='relu', kernel_initializer='glorot_uniform')(readings)
    target = Dense(1, kernel_initializer='glorot_uniform')(x)
    
    model = Model(inputs=[readings], outputs=[target])
    model.compile(loss='mse', optimizer="adam")
    
    return model
    ##################### End of Model
    
def DNN_Model_2(X_train, y_train, LEARNING_RATE):
    model = Sequential()
    model.add(Dense(256, kernel_initializer='glorot_normal', activation='relu', input_dim=X_train.shape[1]))
    model.add(Dense(128, kernel_initializer='glorot_normal', activation='relu'))
    model.add(Dense(1))
    
    adam = optimizers.adam(lr=LEARNING_RATE)
    model.compile(loss='mse', optimizer=adam)
    return model


## define the model :https://machinelearningmastery.com/regression-tutorial-keras-deep-learning-library-python/
def larger_model():
	# create model
	model = Sequential()
	model.add(Dense(13, input_dim=48, kernel_initializer='normal', activation='relu'))
	model.add(Dense(6, kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, kernel_initializer='normal'))
	# Compile model
	model.compile(loss='mean_squared_error', optimizer='adam')
	return model

#### Make  matrix of dataset, standerscaler and, Split "target variable": y and "Inputs Variable": X
def func(df):
    Xorg = df.as_matrix()  # Take one dataset: hm
    
    #######
    YY = Xorg[:, 48]
    XX = Xorg[:, 0:48]
    ######
    
    scaler = StandardScaler()
    Xscaled = scaler.fit_transform(Xorg)
    ## store these off for predictions with unseen data
    Xmeans = scaler.mean_
    Xstds = scaler.scale_
    
    y = Xscaled[:, 48]
    X = np.delete(Xscaled, 48, axis=1)
    
    train_size = int(0.8 * X.shape[0])
    X_train, X_test, y_train, y_test = X[0:train_size], X[train_size:], y[0:train_size], y[train_size:]
    
    ##################################################################
    seed = 44
    np.random.seed(seed)
    estimators = []
    estimators.append(('standardize', StandardScaler()))
    estimators.append(('mlp', KerasRegressor(build_fn=larger_model, epochs=50, batch_size=5, verbose=0)))
    pipeline = Pipeline(estimators)
    kfold = KFold(n_splits=10, random_state=seed)
    results = cross_val_score(pipeline, XX, YY, cv=kfold)
    print("Larger: %.2f (%.2f) MSE" % (results.mean(), results.std()))
    
    ###################################################################
    
    """ 
    ## Call deep NN Function named: DNN_Model(_, _)
    seed = 44
    np.random.seed(seed)
    
    NUM_EPOCHS = 20
    BATCH_SIZE = 10
    
    model = DNN_Model(X_train, y_train)
    history = model.fit(X_train, y_train, batch_size=BATCH_SIZE, epochs=NUM_EPOCHS, validation_split=0.2)
    """
    ##############################################################################
    """
    ## Call deep NN Function named: DNN_Model_2(_, _, _)
    seed = 44
    np.random.seed(seed)
    
    BATCH_SIZE = 64
    EPOCHS = 100
    LEARNING_RATE = 0.0003
    
    model = DNN_Model_2(X_train, y_train, LEARNING_RATE)
    history = model.fit(x=X_train, y=y_train, batch_size=BATCH_SIZE, epochs=EPOCHS, 
                    verbose=1, validation_data=(X_test, y_test))
    """
    ##########################################################################
    '''
    val_predictions = model.predict(X_test).flatten()
    mse = mean_squared_error( ((val_predictions * Xstds[X_train.shape[1]]) + Xmeans[X_train.shape[1]]),
                              ((y_test * Xstds[X_train.shape[1]]) + Xmeans[X_train.shape[1]]) )
    rmse = np.sqrt(mse)
    r2_square =  r2_score( ((y_test * Xstds[X_train.shape[1]]) + Xmeans[X_train.shape[1]]) ,
                           ((val_predictions * Xstds[X_train.shape[1]]) + Xmeans[X_train.shape[1]]) ) ## coefficient_of_dermination 
    
    print('Model validation metrics')
    print('MSE: %.2f' % mse)
    print('RMSE: %.2f' % rmse)
    print('R^2 : %.2f' % r2_square)
    
    plot_metrics(history.history['loss'], history.history['val_loss'])
    '''
    ######################################################################################
    
    '''
    ### Show actual and prdicted values.
    y_test_ = model.predict(X_test).flatten()
    for i in range(X_test.shape[0]):
        label = (y_test[i] * Xstds[X_train.shape[1]]) + Xmeans[X_train.shape[1]] # X_train.shape[1] = 48
        pred = (y_test_[i] * Xstds[X_train.shape[1]]) + Xmeans[X_train.shape[1]]
        print("Exp: {:.3f}, pred: {:.3f}".format(label, pred))
    
    #### plot actual and pred.
    plt.plot(np.arange(y_test.shape[0]), (y_test * Xstds[X_train.shape[1]]) / Xmeans[X_train.shape[1]], color='b', label='actual')
    plt.plot(np.arange(y_test_.shape[0]), (y_test_ * Xstds[X_train.shape[1]]) / Xmeans[X_train.shape[1]], color='r', alpha=0.5, label='predicted')
    plt.xlabel("Time")
    plt.ylabel("labels")
    plt.legend(loc="best")
    plt.show()
    '''

if __name__ == '__main__':
#    print("For LED-0850: GL")
    func(df_NO_hm)
    
    