#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 19:46:49 2018

@author: rezwan
"""

######### Import necessaries libraries.
import random
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C



#### read the .csv files
df_0850_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/df_0850_gl_new.csv")
df_0850_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/df_0850_hemo_new.csv")
df_0850_spo2 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/df_0850_spo2_pre.csv")

df_0940_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/df_0940_gl_new.csv")
df_0940_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/df_0940_hemo_new.csv")
df_0940_spo2 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/df_0940_spo2_pre.csv")

df_1070_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/df_1070_gl_new.csv")
df_1070_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/df_1070_hemo_new.csv")
df_1070_spo2 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/df_1070_spo2_pre.csv")

df_NO_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/df_NO_gl_new.csv")
df_NO_hm = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/df_NO_hemo_new.csv")
df_NO_spo2 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/df_NO_spo2_pre.csv")


##### remove ID from the datasets
del df_0850_gl["ID"]
del df_0850_hm["ID"]
del df_0850_spo2["ID"]

del df_0940_gl["ID"]
del df_0940_hm["ID"]
del df_0940_spo2["ID"]

del df_1070_gl["ID"]
del df_1070_hm["ID"]
del df_1070_spo2["ID"]

del df_NO_gl["ID"]
del df_NO_hm["ID"]
del df_NO_spo2["ID"]

##================================ Calculte variance of dataset 
#print(df_0850_gl.var())
#print(df_0850_hm.var())
#
#print(df_NO_gl.var())
#print(df_NO_hm.var())
##============================================================


#### import user define function
from learning_model import *
from fitness_function import *
from feature_selection_ga import *

######================ Select the Dataset
df = df_NO_spo2
######=================================


### Split Inout set and Target set.
Xorg = df.as_matrix()  # Take one dataset: i.e, hm

#################################### without Standarscaler
#y = Xorg[:, [48]] # target 
#X = Xorg[:, 0:48] # Inputs
####################################

##============== StandardScaler
scaler = StandardScaler()
Xscaled = scaler.fit_transform(Xorg)
## store these off for predictions with unseen data
Xmeans = scaler.mean_
Xstds = scaler.scale_

y = Xscaled[:, 48]
X = np.delete(Xscaled, 48, axis=1)
#######################################=== Model ======================
#model = MLP_reg()
#model = LinearRegression()
#model = DNN_Model(X, y)
model = SVR(kernel='rbf')
# Instantiate a Gaussian Process model
#kernel = C(1.0, (1e-3, 1e3)) * RBF(10, (1e-2, 1e2))
#model = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=9)

#lin_reg = linear_regression(X, y)
#log_reg = logistic_regression(X, y)
#knn_reg = K_Neighbors_regression(X, y)
#svm_reg = support_vector_regression(X, y)
##================================================= End Model ========================1


####################################=== Split the dataset
#train_size = int(0.8 * X.shape[0])
#X_train, X_test, y_train, y_test = X[0:train_size], X[train_size:], y[0:train_size], y[train_size:]

####================= seed
import random
seed = 42
random.seed(seed)
##===================
#######################################
#    from feature_selection_ga import Feat_sel_GA
fsga = Feat_sel_GA(model, X, y)
pop, best_ind = fsga.generate(200, 150) ### Polulation, Generation
pp = fsga.plot_feature_set_score()
#    print(pop)
#################################=== Set it on Machine learning Model
get_best_ind = []
for i in range(len(best_ind)):
    if best_ind[i] == 1:
        get_best_ind.append(i)
        
print(len(get_best_ind))

X_selct = X[:, get_best_ind]
print(X_selct.shape)

###### Define the Model
#model = SVR()
#model = DNN_Model(X_selct, y)
##=====================

############### Apply 10-fold Cross validation
n_splits = 10
cv_set = np.repeat(-1.,X_selct.shape[0])
skf = KFold(n_splits = n_splits ,shuffle=True, random_state=42)
for train_index,test_index in skf.split(X_selct, y):
    x_train,x_test = X_selct[train_index],X_selct[test_index]
    y_train,y_test = y[train_index],y[test_index]
    if x_train.shape[0] != y_train.shape[0]:
        raise Exception()
    model.fit(x_train,y_train)
    predicted_y = model.predict(x_test)
    print("Individual R^2: " +str(r2_score(y_test, predicted_y)))
    cv_set[test_index] = predicted_y
 
print("Over R^2: " + str(r2_score(y,cv_set)))

### ===== For Get real values
y = (y * Xstds[48]) + Xmeans[48]
cv_set = (cv_set * Xstds[48]) + Xmeans[48]
### ===============

print("Over R^2: " + str(r2_score(y,cv_set)))




#if __name__ == '__main__':
#    print("Here you can pass the dataset.")
#    __func(df_0850_spo2)