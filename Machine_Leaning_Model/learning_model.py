#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 21:18:30 2018

@author: rezwan
"""
"""
Source: 
    (1) https://github.com/rezwanh001/PPG/blob/master/ppg/learn.py


"""
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier, VotingClassifier
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, Dropout, BatchNormalization, Activation, Dropout
from keras import optimizers
from sklearn.metrics import r2_score
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn import metrics
from sklearn.neural_network import MLPClassifier, MLPRegressor

def MLP_reg():
    mlp = MLPRegressor(hidden_layer_sizes=(100, ), activation='relu', solver='adam', alpha=0.0001, batch_size='auto')
    return mlp

def linear_regression(features, labels):
    parameters = {'fit_intercept':[True,False], 
                  'normalize':[True,False], 'copy_X':[True, False]
    }
    model = GridSearchCV(LinearRegression(), parameters, n_jobs=-1)
    model.fit(features, labels)
    return model

def logistic_regression(features, labels):
    parameters = {
            'penalty': 'l2',  'C': [1.0, 10.0, 100.0],
            'solver': ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga']
    }
    model = GridSearchCV(LogisticRegression(random_state=1), parameters, n_jobs=-1)
    model.fit(features, labels)
    return model

def K_Neighbors_regression(features, labels):
    params = {'n_neighbors':[2,3,4,5,6,7,8,9,11,13,15,17,19]}

    knn = KNeighborsRegressor()
    
    model = GridSearchCV(knn, params, n_jobs=-1)
    model.fit(features, labels)
    return model
    

def support_vector_regression(features, labels):
    parameters = {
        'C': [1.0, 10.0, 100.0],
        'kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
        'degree': [2,3,4,5]
    }
    model = GridSearchCV(SVR(gamma='scale', epsilon=0.2), parameters, n_jobs=-1)
    model.fit(features, labels)
    return model


def gaussian_naive_bayes_classifier(features, labels):
    classifier = GaussianNB()
    classifier.fit(features, labels)
    return classifier


def decision_tree_classifier(features, labels):
    parameters = {
        'max_depth': [None] + range(1, 11, 1),
    }
    classifier = GridSearchCV(DecisionTreeClassifier(random_state=1), parameters, n_jobs=-1)
    classifier.fit(features, labels)
    return classifier


def random_forest_classifier(features, labels):
    parameters = {
        'n_estimators': range(10, 201, 10),
        'max_depth': [None] + range(1, 11, 1),
    }
    classifier = GridSearchCV(RandomForestClassifier(random_state=1), parameters, n_jobs=-1)
    classifier.fit(features, labels)
    return classifier


def adaboost_classifier(features, labels):
    parameters = {
        'n_estimators': range(50, 201, 10),
        'learning_rate': [float(x) / 10.0 for x in range(1, 11, 1)],
    }
    classifier = GridSearchCV(AdaBoostClassifier(random_state=1), parameters, n_jobs=-1)
    classifier.fit(features, labels)
    return classifier


def gradient_boosting_classifier(features, labels):
    parameters = {
        'learning_rate': [float(x) / 10.0 for x in range(1, 11, 1)],
        'n_estimators': range(50, 201, 10),
        'max_depth': range(1, 11, 1),
    }
    classifier = GridSearchCV(GradientBoostingClassifier(random_state=1), parameters, n_jobs=-1)
    classifier.fit(features, labels)
    return classifier


def voting_classifier(estimators, features, labels):
    parameters = {
        'voting': ['soft', 'hard'],
    }
    classifier = GridSearchCV(VotingClassifier(estimators=estimators), parameters, n_jobs=-1)
    classifier.fit(features, labels)
    return classifier

def DNN_Model(X_train, y_train, LEARNING_RATE = 0.0003):
    model = Sequential()
    model.add(Dense(256, kernel_initializer='glorot_normal', activation='relu', input_dim=X_train.shape[1]))
    model.add(Dense(128, kernel_initializer='glorot_normal', activation='relu'))
    model.add(Dense(1))
    
    adam = optimizers.adam(lr=LEARNING_RATE)
    model.compile(loss='mse', optimizer=adam)
    return model


##=============================== SVR =============================
"""
import pandas as pd
import numpy as np

df = pd.read_csv("/home/rezwan/Documents/sample.csv")

df = df.replace('?', np.nan)

print(df.dtypes)

df.C = df.C.apply(pd.to_numeric)
df.D = df.D.apply(pd.to_numeric)

print(df.dtypes)

df.C = df.C.fillna(df.groupby('types')['C'].transform('median'))
df.D = df.D.fillna(df.groupby('types')['D'].transform('median'))
"""