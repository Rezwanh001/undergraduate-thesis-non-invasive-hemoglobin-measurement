#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 10:26:24 2018

@author: rezwan
"""

from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import KFold
import numpy as np
from sklearn.metrics import f1_score, r2_score, mean_squared_error


class FitenessFunction:
    
    def __init__(self,n_splits = 10,*args,**kwargs):
        """
            Parameters
            -----------
            n_splits :int, 
                Number of splits for cv
            
            verbose: 0 or 1
        """
        self.n_splits = n_splits
        self.r2_scores = []
    

    def calculate_fitness(self,model,x,y):
        cv_set = np.repeat(-1.,x.shape[0])
#        skf = StratifiedKFold(n_splits = self.n_splits, shuffle = True, random_state=999)
        skf = KFold(n_splits = self.n_splits, shuffle = True, random_state=42)
        for train_index,test_index in skf.split(x,y):
            x_train,x_test = x[train_index],x[test_index]
            y_train,y_test = y[train_index],y[test_index]
            if x_train.shape[0] != y_train.shape[0]:
                raise Exception()
            model.fit(x_train,y_train)
            predicted_y = model.predict(x_test)
            ##================================
#            print(r2_score(y_test, predicted_y))
            self.r2_scores.append(r2_score(y_test, predicted_y))
            ##================================
            cv_set[test_index] = predicted_y
        print("Best R^2: " +str(max(self.r2_scores)))
        return r2_score(y,cv_set)