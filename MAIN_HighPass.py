#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 16 18:41:36 2018

@author: rezwan
"""

from libraries import *
from video_to_frames import *
from plot_peak_frq import *
from plot_time_series import *
from high_pass_filter import *
from FFT import *
from peakdetect import *


##################################################################################
'''                R Channel , G Channel, B Channel   '''
##################################################################################

'''R channel'''
LED_0850_R = []
LED_0940_R = []
LED_1070_R = []
LED_NO_R = []

ID_LST_R = []

'''G channel'''
LED_0850_G = []
LED_0940_G = []
LED_1070_G = []
LED_NO_G = []

ID_LST_G = []

'''B channel'''
LED_0850_B = []
LED_0940_B = []
LED_1070_B = []
LED_NO_B = []

ID_LST_B = []

""" Generate .csv files for  R, G, B channels respectively . """

#src_dir = "Extract-Images"
src_dir = "RawData/Extract-Images"
LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"] #, "/LED-0940", "/LED-1070","/LED-NO"

for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    
    for folder in folList:
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
        print(len(file_names))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
            
        ''' # Plot the time series of each channel    
        ### Plot the Red channel time series
        plot_time_series(r_mean, "r", "Red Channel")
        
        ### Plot the Green channel time series
        plot_time_series(g_mean, "g", "Green Channel")
        
        ### Plot the Blue channel time series
        plot_time_series(b_mean, "b", "Blue Channel")
        '''
        
        ##### High Pass filter apply on each channel ####################
        fps = 60
        cutoff = 0.5
        high_pass_filter_R = butter_highpass_filter(r_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_R, 'r', label='High pass filter for R channel')
        
        high_pass_filter_G = butter_highpass_filter(g_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_G, 'g', label='High pass filter for G channel')
        
        high_pass_filter_B = butter_highpass_filter(b_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_B, 'b', label='High pass filter for B channel')
        
        
        ############## Fast FFT on high pass filtered channel ###############
        FFT_R = FFT(high_pass_filter_R)
        #plot_time_series(FFT_R, 'r', label='FFT for R channel')
        
        FFT_G = FFT(high_pass_filter_G)
        #plot_time_series(FFT_G, 'g', label='FFT for G channel')
        
        FFT_B = FFT(high_pass_filter_B)
        #plot_time_series(FFT_B, 'b', label='FFT for B channel')
        
        
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
    
        ####### Find Dominant Peak From Filtered channels ##############
        '''
        R Channel
        '''
        y = high_pass_filter_R
        #y = FFT_R
        x = range(len(high_pass_filter_R))
        _max, _min = peakdetect(y,x,1, 0.0)
        #print(_max, _min)
        max_peak = abs(max(_max,key=lambda item:item[1])[1])
        min_peak = abs(min(_min,key=lambda item:item[1])[1])
        log_of_max_min = np.log(min_peak / max_peak)       # ln(min/max)
#        log_of_max_min = (max_peak / min_peak)              # (max/min)
    #    print("Max dominant for R : " + str(max(_max,key=lambda item:item[1])[1]))
    #    print("Min dominant for R : " + str(min(_min,key=lambda item:item[1])[1]))
        print("Log of Min/Max peak for R : " + str(log_of_max_min))
    #    xm = [p[0] for p in _max]
    #    ym = [p[1] for p in _max]
    #    xn = [p[0] for p in _min]
    #    yn = [p[1] for p in _min]
    #    plot_peak_frq(y,x, xm, xn, ym, yn, "r", "Peak for R channel")
        
        if LED_Broad == '0850':
            LED_0850_R.append(log_of_max_min)
        elif LED_Broad == '0940':
            LED_0940_R.append(log_of_max_min)
        elif LED_Broad == '1070':
            LED_1070_R.append(log_of_max_min)
        elif LED_Broad == 'NO':
            LED_NO_R.append(log_of_max_min)
            
        if ID not in ID_LST_R:
            ID_LST_R.append(ID)
        
        '''
        G Channel
        '''
        y = high_pass_filter_G
        #y = FFT_R
        x = range(len(high_pass_filter_G))
        _max, _min = peakdetect(y,x,1, 0.0)
        #print(_max, _min)
        max_peak = abs(max(_max,key=lambda item:item[1])[1])
        min_peak = abs(min(_min,key=lambda item:item[1])[1])
        log_of_max_min = np.log(min_peak / max_peak)       # ln(min/max)
#        log_of_max_min = (max_peak / min_peak)              # (max/min)
    #    print("Max dominant for G : " + str(max(_max,key=lambda item:item[1])[1]))
    #    print("Min dominant for G : " + str(min(_min,key=lambda item:item[1])[1]))
        print("Log of Min/Max peak for G : " + str(log_of_max_min))
    #    xm = [p[0] for p in _max]
    #    ym = [p[1] for p in _max]
    #    xn = [p[0] for p in _min]
    #    yn = [p[1] for p in _min]
    #    plot_peak_frq(y,x, xm, xn, ym, yn, "r", "Peak for R channel")
    
        if LED_Broad == '0850':
            LED_0850_G.append(log_of_max_min)
        elif LED_Broad == '0940':
            LED_0940_G.append(log_of_max_min)
        elif LED_Broad == '1070':
            LED_1070_G.append(log_of_max_min)
        elif LED_Broad == 'NO':
            LED_NO_G.append(log_of_max_min)
            
        if ID not in ID_LST_G:
            ID_LST_G.append(ID)
    
            
        '''
        B Channel
        '''
        y = high_pass_filter_B
        #y = FFT_R
        x = range(len(high_pass_filter_B))
        _max, _min = peakdetect(y,x,1, 0.0)
        #print(_max, _min)
        max_peak = abs(max(_max,key=lambda item:item[1])[1])
        min_peak = abs(min(_min,key=lambda item:item[1])[1])
        log_of_max_min = np.log(min_peak / max_peak)       # ln(min/max)
#        log_of_max_min = (max_peak / min_peak)              # (max/min)
    #    print("Max dominant for B : " + str(max(_max,key=lambda item:item[1])[1]))
    #    print("Min dominant for B : " + str(min(_min,key=lambda item:item[1])[1]))
        print("Log of Min/Max peak for B : " + str(log_of_max_min))
    #    xm = [p[0] for p in _max]
    #    ym = [p[1] for p in _max]
    #    xn = [p[0] for p in _min]
    #    yn = [p[1] for p in _min]
    #    plot_peak_frq(y,x, xm, xn, ym, yn, "r", "Peak for R channel")
    
        if LED_Broad == '0850':
            LED_0850_B.append(log_of_max_min)
        elif LED_Broad == '0940':
            LED_0940_B.append(log_of_max_min)
        elif LED_Broad == '1070':
            LED_1070_B.append(log_of_max_min)
        elif LED_Broad == 'NO':
            LED_NO_B.append(log_of_max_min)
            
        if ID not in ID_LST_B:
            ID_LST_B.append(ID)
        
    
        
        
print(ID_LST_R)
print(LED_0850_R)
print(LED_0940_R)
print(LED_1070_R)
print(LED_NO_R)

'''Create the DataFrame for R channel'''
dataFrame_R_channel = pd.DataFrame({
    'Sub_ID': ID_LST_R,
    'ln(Min/Max)_0850': LED_0850_R,
    'ln(Min/Max)_0940': LED_0940_R,
    'ln(Min/Max)_1070': LED_1070_R,
    'ln(Min/Max)_NO': LED_NO_R
})
dataFrame_R_channel.to_csv('CSV_Files/High_Pass_Filter/R_channel_HighPass_ln(Min_Max).csv')


print(ID_LST_G)
print(LED_0850_G)
print(LED_0940_G)
print(LED_1070_G)
print(LED_NO_G)

'''Create the DataFrame for G channel'''
dataFrame_G_channel = pd.DataFrame({
    'Sub_ID': ID_LST_G,
    'ln(Min/Max)_0850': LED_0850_G,
    'ln(Min/Max)_0940': LED_0940_G,
    'ln(Min/Max)_1070': LED_1070_G,
    'ln(Min/Max)_NO': LED_NO_G
})
dataFrame_G_channel.to_csv('CSV_Files/High_Pass_Filter/G_channel_HighPass_ln(Min_Max).csv')




print(ID_LST_B)
print(LED_0850_B)
print(LED_0940_B)
print(LED_1070_B)
print(LED_NO_B)

'''Create the DataFrame for B channel'''
dataFrame_B_channel = pd.DataFrame({
    'Sub_ID': ID_LST_B,
    'ln(Min/Max)_0850': LED_0850_B,
    'ln(Min/Max)_0940': LED_0940_B,
    'ln(Min/Max)_1070': LED_1070_B,
    'ln(Min/Max)_NO': LED_NO_B
})
dataFrame_B_channel.to_csv('CSV_Files/High_Pass_Filter/B_channel_HighPass_ln(Min_Max).csv')
    
