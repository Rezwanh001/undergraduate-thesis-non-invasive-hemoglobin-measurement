#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 11:38:18 2018

@author: rezwan
"""

from libraries import *
from plot_time_series import *
from bandPass import *
from peakdetection import peakdet
from feature_PPG_N_Back_Clip import *
from max_first_three import *
from params import *
from feature import extract_ppg45, extract_svri


""" Generate .csv files for  R, G, B channels respectively . """

##############################
#   Select the directory    #
##############################
#src_dir = "Extract-Images"  
src_dir = "RawData/Extract-Images"


                    ######################################
                    #          /LED-0850                #
                    #####################################

LED_Broad_num = ["/LED-0850"]

#LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"] #, "/LED-0940", "/LED-1070","/LED-NO"

PPG_45_feat_0850_Red = []

for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    print("Path : " + str(path))
    
    
    idx = 0 ##################
    
    for folder in folList:
        print("Folder : " + str(folder))
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
#        print(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                # print("Image size: " + str(img.shape))
                
                '''
                 #to get a square centered in the middle
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1]//2 - 250 # width//2 - 250. where width = 1920 px
                img = img[h:h+500, w:w+500]
                # img = img[290:790, 710:1210]
                '''
                
                #to get a square right to left
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1] # width. where width = 1920 px
                img = img[h:h+500, w-500:w]
                
                # print("Image size: " + str(img.shape))
                
                #print(file[:-4])
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                # print("Image size: " + str(img.shape))
                
                '''
                 #to get a square centered in the middle
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1]//2 - 250 # width//2 - 250. where width = 1920 px
                img = img[h:h+500, w:w+500]
                # img = img[290:790, 710:1210]
                '''
                
                #to get a square right to left
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1] # width. where width = 1920 px
                img = img[h:h+500, w-500:w]
                
                # print("Image size: " + str(img.shape))
                
                #print(file[:-4])
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
                
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
        #############################
        print(max(r_mean), min(r_mean))
        print(max(g_mean), min(g_mean))
        print(max(b_mean), min(b_mean))
        ##================ Plot all channel
        plot_all_chan(r_mean, g_mean, b_mean)

        ##################################################
        #           R channel
        ##################################################
        #plot_time_series(r_mean, "r", "R channel")
        R_bandPass = bandPass(r_mean)
        R_bandPass = R_bandPass[::-1]
        #plot_time_series(R_bandPass, "r", "BandPass For R channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = R_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
#        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-0850/Red_img/"+str(ID)+"_"+str(LED_Broad)+"_red.png", dpi = 100)
        plt.show()
        
        
        ''' Pick Fresh 3 PPG wave '''
        from pyexcel_ods import get_data
        data = get_data("/media/rezwan/Softwares/THESIS/PPG_Img/LED-0850_RED.ods")
#        print(data)
        
        Red_2d_list = list(data.values())[0][1:]
        #print(Red_2d_list)
        
        Id = Red_2d_list[idx][0]
#        print(Id)
        idx_Red_2d_list = Red_2d_list[idx]
#        print(idx_Red_2d_list)
        
        Red_PPG_Clean = [] 
        for i in range(1, len(idx_Red_2d_list)-1, 2):
#            print(idx_Red_2d_list[i], idx_Red_2d_list[i+1])
            Red_PPG_Clean.append(list(series[idx_Red_2d_list[i]:idx_Red_2d_list[i+1]+1]))
        
#        print(list(Red_PPG_Clean))
        Red_PPG_Clean = sum(Red_PPG_Clean, []) ## Here we take the 3 PPG wave
#        Red_PPG_Clean = Red_PPG_Clean[0] ## Here, We take the best PPG wave.
        
        plot_time_series_n_save_0850(Red_PPG_Clean, 'k', "Cleaned PPG Wave", Id, LED_Broad)
        idx += 1
        
        """ PPG-45 + extract_svri Feature Extraction """
        ppg_feat_45 = extract_ppg45(Red_PPG_Clean)
        svri = extract_svri(Red_PPG_Clean)
        
        ppg_feat_45.insert(0, int(ID))
        ppg_feat_45.append(svri)
        PPG_45_feat_0850_Red.append(ppg_feat_45)
        
       
        
 
"""         
        ##################################################
        #           Green channel
        ##################################################
        # plot_time_series(g_mean, "g", "G channel")
        G_bandPass = bandPass(g_mean)
        G_bandPass = G_bandPass[::-1]
#        plot_time_series(G_bandPass, "g", "BandPass For G channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = G_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-0850/Green_img/"+str(ID)+"_green.png", dpi = 100)
        plt.show()
        
        
        ##################################################
        #           Blue channel
        ##################################################
        # plot_time_series(b_mean, "b", "B channel")
        B_bandPass = bandPass(b_mean)
        B_bandPass = B_bandPass[::-1]
#        plot_time_series(B_bandPass, "b", "BandPass For B channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = B_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-0850/Blue_img/"+str(ID)+"_blue.png", dpi = 100)
        plt.show()
""" 


""" 
Preprocess the dataset for LED-0850_red
"""
headers = ['ID', 'Systolic_peak(x)', 'Diastolic_peak(y)', 'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)', 'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x', 'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)', 'Dicrotic_notch_time(t3)', 'Time_between_systolic_and_diastolic_peaks(∆T)', 'Time_between_half_systolic_peak_points(w)', 'Inflection_point_area_ratio(A2/A1)', 'Systolic_peak_rising_slope(t1/x)', 'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi', 't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2', '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi', 'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi', '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)', 'Fundamental_component_magnitude(|sbase|)', '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)', '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)', 'Stress-induced_vascular_response_index(sVRI)']       

df = pd.DataFrame(PPG_45_feat_0850_Red, columns=headers)
df.to_csv('CSV_Files/LED-0850_PPG_CSV/PPG_45_feat_0850_Red.csv')      

''' Merge PPG_45_feat_0850_Red and Clinical data '''
import numpy as np
import pandas as pd
df_Clinical = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Shared AmaderGram Data 2018-DataSheet - Clinical data.csv")
df_PPG_45_feat_0850_Red = pd.read_csv('CSV_Files/LED-0850_PPG_CSV/PPG_45_feat_0850_Red.csv')

df_PPG_45_feat_0850_Red_merge = pd.merge(df_PPG_45_feat_0850_Red, df_Clinical, on='ID', how='outer')

col_list = ['ID', 'Age', 'Sex', 'Systolic_peak(x)', 'Diastolic_peak(y)',
       'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)',
       'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x',
       'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)',
       'Dicrotic_notch_time(t3)',
       'Time_between_systolic_and_diastolic_peaks(∆T)',
       'Time_between_half_systolic_peak_points(w)',
       'Inflection_point_area_ratio(A2/A1)',
       'Systolic_peak_rising_slope(t1/x)',
       'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi',
       't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2',
       '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi',
       'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi',
       '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)',
       'Fundamental_component_magnitude(|sbase|)',
       '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)',
       '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)',
       'Stress-induced_vascular_response_index(sVRI)',  'Hb (gm/dL)']
print(len(col_list))

df_PPG_45_feat_0850_Red_merge = df_PPG_45_feat_0850_Red_merge[col_list]

dict = {'Sex':{'Male':1, 'Female':0}}      # label = column name
df_PPG_45_feat_0850_Red_merge.replace(dict,inplace = True) 


'''If any missing value occurs in the "Stress-induced_vascular_response_index(sVRI)", 
then Handing this.
'''
# Any missing values?
print(df_PPG_45_feat_0850_Red_merge.isnull().values.any())

# Total missing values for each feature
print(df_PPG_45_feat_0850_Red_merge.isnull().sum())

# Looking at the Stress-induced_vascular_response_index(sVRI) column
print(df_PPG_45_feat_0850_Red_merge['Stress-induced_vascular_response_index(sVRI)'].isnull())

# Replace using median 
median = df_PPG_45_feat_0850_Red_merge['Stress-induced_vascular_response_index(sVRI)'].median()
print(median)

# Replace using mean 
mean = df_PPG_45_feat_0850_Red_merge['Stress-induced_vascular_response_index(sVRI)'].mean()
print(mean)

#Find index of all rows with null values in a particular column in pandas dataframe
miss_lst = df_PPG_45_feat_0850_Red_merge[df_PPG_45_feat_0850_Red_merge['Stress-induced_vascular_response_index(sVRI)'].isnull()].index.tolist()
print(miss_lst)

# Location based replacement: replace with mean
df_PPG_45_feat_0850_Red_merge.loc[miss_lst,'Stress-induced_vascular_response_index(sVRI)'] = mean

# Total missing values for each feature
print(df_PPG_45_feat_0850_Red_merge.isnull().sum())


''' Create Finale Dataset '''
df_PPG_45_feat_0850_Red_merge.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Final_dataset/Pre_df_PPG_45_feat_0850_Red_merge.csv")


#''' Eliminate the wrong PPG cycle. '''
#df_0850_hemo = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_0850_Red_Hemo.csv")
#df_0850_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_0850_Red_Gl.csv")
#df_0850_hemo_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_0850_Red_merge.csv")
#
### Load the text file where put the wrong ID
#with open('/media/rezwan/Softwares/THESIS/PPG_Img/LED-0850.txt', 'r') as f:
#    lines = f.readlines()
#    
#junk_list_0850_red = []
#for e in lines:
#    for n in e.split(","):
#        junk_list_0850_red.append(int(n))
#    
#print(len(junk_list_0850_red))
#
#
### Eliminate the Id from the dataframe that indicates junk_list_0850_red
###### Hemoglobin
#print(df_0850_hemo.describe())
#print(df_0850_hemo.shape)
#print(df_0850_hemo['ID'])
#print(len(df_0850_hemo['ID']))
#
#df_0850_hemo_new = df_0850_hemo[~df_0850_hemo['ID'].isin(junk_list_0850_red)]
#
#print(df_0850_hemo_new.describe())
#print(df_0850_hemo_new.shape)
#print(df_0850_hemo_new['ID'])
#print(len(df_0850_hemo_new['ID']))
#
#df_0850_hemo_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Final_dataset/df_0850_hemo.csv")
#
#
###### Glucose
#print(df_0850_gl.describe())
#print(df_0850_gl.shape)
#print(df_0850_gl['ID'])
#print(len(df_0850_gl['ID']))
#
#df_0850_gl_new = df_0850_gl[~df_0850_gl['ID'].isin(junk_list_0850_red)]
#
#print(df_0850_gl_new.describe())
#print(df_0850_gl_new.shape)
#print(df_0850_gl_new['ID'])
#print(len(df_0850_gl_new['ID']))
#
#df_0850_gl_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Final_dataset/df_0850_gl.csv")
#
###### Hemo and Glucose
#print(df_0850_hemo_gl.describe())
#print(df_0850_hemo_gl.shape)
#print(df_0850_hemo_gl['ID'])
#print(len(df_0850_hemo_gl['ID']))
#
#df_0850_hemo_gl_new = df_0850_hemo_gl[~df_0850_hemo_gl['ID'].isin(junk_list_0850_red)]
#
#print(df_0850_hemo_gl_new.describe())
#print(df_0850_hemo_gl_new.shape)
#print(df_0850_hemo_gl_new['ID'])
#print(len(df_0850_hemo_gl_new['ID']))
#
#df_0850_hemo_gl_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/Final_dataset/df_0850_hemo_gl.csv")
#



                    ######################################
                    #          /LED-0940                #
                    #####################################

LED_Broad_num = ["/LED-0940"]

PPG_45_feat_0940_Red = []

for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    print("Path : " + str(path))
    
    idx = 0 ##################
    
    for folder in folList:
        print("Folder : " + str(folder))
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
#        print(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                # print("Image size: " + str(img.shape))
                
                '''
                 #to get a square centered in the middle
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1]//2 - 250 # width//2 - 250. where width = 1920 px
                img = img[h:h+500, w:w+500]
                # img = img[290:790, 710:1210]
                '''
                
                #to get a square right to left
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1] # width. where width = 1920 px
                img = img[h:h+500, w-500:w]
                
                # print("Image size: " + str(img.shape))
                
                #print(file[:-4])
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                # print("Image size: " + str(img.shape))
                
                '''
                 #to get a square centered in the middle
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1]//2 - 250 # width//2 - 250. where width = 1920 px
                img = img[h:h+500, w:w+500]
                # img = img[290:790, 710:1210]
                '''
                
                #to get a square right to left
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1] # width. where width = 1920 px
                img = img[h:h+500, w-500:w]
                
                # print("Image size: " + str(img.shape))
                
                #print(file[:-4])
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
                
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
        #############################

        ##################################################
        #           R channel
        ##################################################
        # plot_time_series(r_mean, "r", "R channel")
        R_bandPass = bandPass(r_mean)
        R_bandPass = R_bandPass[::-1]
#        plot_time_series(R_bandPass, "r", "BandPass For R channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = R_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
#        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-0940/Red_img/"+str(ID)+"_"+str(LED_Broad)+"_red.png", dpi = 100)
        plt.show()
        
        ''' Pick Fresh 3 PPG wave '''
        from pyexcel_ods import get_data
        data = get_data("/media/rezwan/Softwares/THESIS/PPG_Img/LED-0940_RED.ods")
#        print(data)
        
        Red_2d_list = list(data.values())[0][1:]
        #print(Red_2d_list)
        
        Id = Red_2d_list[idx][0]
#        print(Id)
        idx_Red_2d_list = Red_2d_list[idx]
#        print(idx_Red_2d_list)
        
        Red_PPG_Clean = [] 
        for i in range(1, len(idx_Red_2d_list)-1, 2):
#            print(idx_Red_2d_list[i], idx_Red_2d_list[i+1])
            Red_PPG_Clean.append(list(series[idx_Red_2d_list[i]:idx_Red_2d_list[i+1]+1]))
        
#        print(list(Red_PPG_Clean))
        Red_PPG_Clean = sum(Red_PPG_Clean, [])
        plot_time_series_n_save_0940(Red_PPG_Clean, 'k', "Cleaned PPG Wave", Id, LED_Broad)
        idx += 1
        
        """ PPG-45 + extract_svri Feature Extraction """
        ppg_feat_45 = extract_ppg45(Red_PPG_Clean)
        svri = extract_svri(Red_PPG_Clean)
        
        ppg_feat_45.insert(0, int(ID))
        ppg_feat_45.append(svri)
        PPG_45_feat_0940_Red.append(ppg_feat_45)
        
        
        
        
"""     
        
        ##################################################
        #           Green channel
        ##################################################
        # plot_time_series(g_mean, "g", "G channel")
        G_bandPass = bandPass(g_mean)
        G_bandPass = G_bandPass[::-1]
#        plot_time_series(G_bandPass, "g", "BandPass For G channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = G_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-0940/Green_img/"+str(ID)+"_green.png", dpi = 100)
        plt.show()
        
        
        ##################################################
        #           Blue channel
        ##################################################
        # plot_time_series(b_mean, "b", "B channel")
        B_bandPass = bandPass(b_mean)
        B_bandPass = B_bandPass[::-1]
#        plot_time_series(B_bandPass, "b", "BandPass For B channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = B_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-0940/Blue_img/"+str(ID)+"_blue.png", dpi = 100)
        plt.show()

"""

""" 
Preprocess the dataset for LED-0940_red
"""

headers = ['ID', 'Systolic_peak(x)', 'Diastolic_peak(y)', 'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)', 'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x', 'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)', 'Dicrotic_notch_time(t3)', 'Time_between_systolic_and_diastolic_peaks(∆T)', 'Time_between_half_systolic_peak_points(w)', 'Inflection_point_area_ratio(A2/A1)', 'Systolic_peak_rising_slope(t1/x)', 'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi', 't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2', '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi', 'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi', '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)', 'Fundamental_component_magnitude(|sbase|)', '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)', '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)', 'Stress-induced_vascular_response_index(sVRI)']       

df = pd.DataFrame(PPG_45_feat_0940_Red, columns=headers)
df.to_csv('CSV_Files/LED-0940_PPG_CSV/PPG_45_feat_0940_Red.csv')      

''' Merge PPG_45_feat_0940_Red and Clinical data '''
import numpy as np
import pandas as pd
df_Clinical = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Shared AmaderGram Data 2018-DataSheet - Clinical data.csv")
df_PPG_45_feat_0940_Red = pd.read_csv('CSV_Files/LED-0940_PPG_CSV/PPG_45_feat_0940_Red.csv')

df_PPG_45_feat_0940_Red_merge = pd.merge(df_PPG_45_feat_0940_Red, df_Clinical, on='ID', how='outer')

col_list = ['ID', 'Age', 'Sex', 'Systolic_peak(x)', 'Diastolic_peak(y)',
       'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)',
       'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x',
       'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)',
       'Dicrotic_notch_time(t3)',
       'Time_between_systolic_and_diastolic_peaks(∆T)',
       'Time_between_half_systolic_peak_points(w)',
       'Inflection_point_area_ratio(A2/A1)',
       'Systolic_peak_rising_slope(t1/x)',
       'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi',
       't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2',
       '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi',
       'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi',
       '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)',
       'Fundamental_component_magnitude(|sbase|)',
       '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)',
       '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)',
       'Stress-induced_vascular_response_index(sVRI)',  'Hb (gm/dL)']
print(len(col_list))

df_PPG_45_feat_0940_Red_merge = df_PPG_45_feat_0940_Red_merge[col_list]

dict = {'Sex':{'Male':1, 'Female':0}}      # label = column name
df_PPG_45_feat_0940_Red_merge.replace(dict,inplace = True) 


'''If any missing value occurs in the "Stress-induced_vascular_response_index(sVRI)", 
then Handing this.
'''
# Any missing values?
print(df_PPG_45_feat_0940_Red_merge.isnull().values.any())

# Total missing values for each feature
print(df_PPG_45_feat_0940_Red_merge.isnull().sum())

# Looking at the Stress-induced_vascular_response_index(sVRI) column
print(df_PPG_45_feat_0940_Red_merge['Stress-induced_vascular_response_index(sVRI)'].isnull())

# Replace using median 
median = df_PPG_45_feat_0940_Red_merge['Stress-induced_vascular_response_index(sVRI)'].median()
print(median)

# Replace using mean 
mean = df_PPG_45_feat_0940_Red_merge['Stress-induced_vascular_response_index(sVRI)'].mean()
print(mean)

#Find index of all rows with null values in a particular column in pandas dataframe
miss_lst = df_PPG_45_feat_0940_Red_merge[df_PPG_45_feat_0940_Red_merge['Stress-induced_vascular_response_index(sVRI)'].isnull()].index.tolist()
print(miss_lst)

# Location based replacement: replace with mean
df_PPG_45_feat_0940_Red_merge.loc[miss_lst,'Stress-induced_vascular_response_index(sVRI)'] = mean

# Total missing values for each feature
print(df_PPG_45_feat_0940_Red_merge.isnull().sum())


''' Create Finale Dataset '''
df_PPG_45_feat_0940_Red_merge.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Final_dataset/Pre_df_PPG_45_feat_0940_Red_merge.csv")

#
#''' Eliminate the wrong PPG cycle. '''
#df_0940_hemo = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_0940_Red_Hemo.csv")
#df_0940_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_0940_Red_Gl.csv")
#df_0940_hemo_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_0940_Red_merge.csv")
#
### Load the text file where put the wrong ID
#with open('/media/rezwan/Softwares/THESIS/PPG_Img/LED-0940.txt', 'r') as f:
#    lines = f.readlines()
#    
#junk_list_0940_red = []
#for e in lines:
#    for n in e.split(","):
#        junk_list_0940_red.append(int(n))
#    
#print(len(junk_list_0940_red))
#
#
### Eliminate the Id from the dataframe that indicates junk_list_0850_red
###### Hemoglobin
#print(df_0940_hemo.describe())
#print(df_0940_hemo.shape)
#print(df_0940_hemo['ID'])
#print(len(df_0940_hemo['ID']))
#
#df_0940_hemo_new = df_0940_hemo[~df_0940_hemo['ID'].isin(junk_list_0940_red)]
#
#print(df_0940_hemo_new.describe())
#print(df_0940_hemo_new.shape)
#print(df_0940_hemo_new['ID'])
#print(len(df_0940_hemo_new['ID']))
#
#df_0940_hemo_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Final_dataset/df_0940_hemo.csv")
#
#
###### Glucose
#print(df_0940_gl.describe())
#print(df_0940_gl.shape)
#print(df_0940_gl['ID'])
#print(len(df_0940_gl['ID']))
#
#df_0940_gl_new = df_0940_gl[~df_0940_gl['ID'].isin(junk_list_0940_red)]
#
#print(df_0940_gl_new.describe())
#print(df_0940_gl_new.shape)
#print(df_0940_gl_new['ID'])
#print(len(df_0940_gl_new['ID']))
#
#df_0940_gl_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Final_dataset/df_0940_gl.csv")
#
#
###### Hemo and Glucose
#print(df_0940_hemo_gl.describe())
#print(df_0940_hemo_gl.shape)
#print(df_0940_hemo_gl['ID'])
#print(len(df_0940_hemo_gl['ID']))
#
#df_0940_hemo_gl_new = df_0940_hemo_gl[~df_0940_hemo_gl['ID'].isin(junk_list_0940_red)]
#
#print(df_0940_hemo_gl_new.describe())
#print(df_0940_hemo_gl_new.shape)
#print(df_0940_hemo_gl_new['ID'])
#print(len(df_0940_hemo_gl_new['ID']))
#
#df_0940_hemo_gl_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/Final_dataset/df_0940_hemo_gl.csv")






                    ######################################
                    #          /LED-1070                #
                    #####################################

LED_Broad_num = ["/LED-1070"]

PPG_45_feat_1070_Red = []

for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    print("Path : " + str(path))
    
    idx = 0 ############
    
    for folder in folList:
        print("Folder : " + str(folder))
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
#        print(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                # print("Image size: " + str(img.shape))
                
                '''
                 #to get a square centered in the middle
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1]//2 - 250 # width//2 - 250. where width = 1920 px
                img = img[h:h+500, w:w+500]
                # img = img[290:790, 710:1210]
                '''
                
                #to get a square right to left
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1] # width. where width = 1920 px
                img = img[h:h+500, w-500:w]
                
                # print("Image size: " + str(img.shape))
                
                #print(file[:-4])
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                # print("Image size: " + str(img.shape))
                
                '''
                 #to get a square centered in the middle
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1]//2 - 250 # width//2 - 250. where width = 1920 px
                img = img[h:h+500, w:w+500]
                # img = img[290:790, 710:1210]
                '''
                
                #to get a square right to left
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1] # width. where width = 1920 px
                img = img[h:h+500, w-500:w]
                
                # print("Image size: " + str(img.shape))
                
                #print(file[:-4])
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
                
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
        #############################

        ##################################################
        #           R channel
        ##################################################
        # plot_time_series(r_mean, "r", "R channel")
        R_bandPass = bandPass(r_mean)
        R_bandPass = R_bandPass[::-1]
#        plot_time_series(R_bandPass, "r", "BandPass For R channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = R_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
#        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-1070/Red_img/"+str(ID)+"_"+str(LED_Broad)+"_red.png", dpi = 100)
        plt.show()
        
        
        ''' Pick Fresh 3 PPG wave '''
        from pyexcel_ods import get_data
        data = get_data("/media/rezwan/Softwares/THESIS/PPG_Img/LED-1070_RED.ods")
#        print(data)
        
        Red_2d_list = list(data.values())[0][1:]
        #print(Red_2d_list)
        
        Id = Red_2d_list[idx][0]
#        print(Id)
        idx_Red_2d_list = Red_2d_list[idx]
#        print(idx_Red_2d_list)
        
        Red_PPG_Clean = [] 
        for i in range(1, len(idx_Red_2d_list)-1, 2):
#            print(idx_Red_2d_list[i], idx_Red_2d_list[i+1])
            Red_PPG_Clean.append(list(series[idx_Red_2d_list[i]:idx_Red_2d_list[i+1]+1]))
        
#        print(list(Red_PPG_Clean))
        Red_PPG_Clean = sum(Red_PPG_Clean, [])
        plot_time_series_n_save_1070(Red_PPG_Clean, 'k', "Cleaned PPG Wave", Id, LED_Broad)
        idx += 1
        
        """ PPG-45 + extract_svri Feature Extraction """
        ppg_feat_45 = extract_ppg45(Red_PPG_Clean)
        svri = extract_svri(Red_PPG_Clean)
        
        ppg_feat_45.insert(0, int(ID))
        ppg_feat_45.append(svri)
        PPG_45_feat_1070_Red.append(ppg_feat_45)
        
        
        
"""     
        
        ##################################################
        #           Green channel
        ##################################################
        # plot_time_series(g_mean, "g", "G channel")
        G_bandPass = bandPass(g_mean)
        G_bandPass = G_bandPass[::-1]
#        plot_time_series(G_bandPass, "g", "BandPass For G channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = G_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-1070/Green_img/"+str(ID)+"_green.png", dpi = 100)
        plt.show()
        
        
        ##################################################
        #           Blue channel
        ##################################################
        # plot_time_series(b_mean, "b", "B channel")
        B_bandPass = bandPass(b_mean)
        B_bandPass = B_bandPass[::-1]
#        plot_time_series(B_bandPass, "b", "BandPass For B channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = B_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-1070/Blue_img/"+str(ID)+"_blue.png", dpi = 100)
        plt.show()

"""

""" 
Preprocess the dataset for LED-1070_red
"""
headers = ['ID', 'Systolic_peak(x)', 'Diastolic_peak(y)', 'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)', 'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x', 'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)', 'Dicrotic_notch_time(t3)', 'Time_between_systolic_and_diastolic_peaks(∆T)', 'Time_between_half_systolic_peak_points(w)', 'Inflection_point_area_ratio(A2/A1)', 'Systolic_peak_rising_slope(t1/x)', 'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi', 't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2', '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi', 'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi', '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)', 'Fundamental_component_magnitude(|sbase|)', '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)', '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)', 'Stress-induced_vascular_response_index(sVRI)']       

df = pd.DataFrame(PPG_45_feat_1070_Red, columns=headers)
df.to_csv('CSV_Files/LED-1070_PPG_CSV/PPG_45_feat_1070_Red.csv')      

''' Merge PPG_45_feat_1070_Red and Clinical data '''
import numpy as np
import pandas as pd
df_Clinical = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Shared AmaderGram Data 2018-DataSheet - Clinical data.csv")
df_PPG_45_feat_1070_Red = pd.read_csv('CSV_Files/LED-1070_PPG_CSV/PPG_45_feat_1070_Red.csv')

df_PPG_45_feat_1070_Red_merge = pd.merge(df_PPG_45_feat_1070_Red, df_Clinical, on='ID', how='outer')

col_list = ['ID', 'Age', 'Sex', 'Systolic_peak(x)', 'Diastolic_peak(y)',
       'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)',
       'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x',
       'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)',
       'Dicrotic_notch_time(t3)',
       'Time_between_systolic_and_diastolic_peaks(∆T)',
       'Time_between_half_systolic_peak_points(w)',
       'Inflection_point_area_ratio(A2/A1)',
       'Systolic_peak_rising_slope(t1/x)',
       'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi',
       't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2',
       '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi',
       'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi',
       '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)',
       'Fundamental_component_magnitude(|sbase|)',
       '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)',
       '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)',
       'Stress-induced_vascular_response_index(sVRI)',  'Hb (gm/dL)']
print(len(col_list))

df_PPG_45_feat_1070_Red_merge = df_PPG_45_feat_1070_Red_merge[col_list]

dict = {'Sex':{'Male':1, 'Female':0}}      # label = column name
df_PPG_45_feat_1070_Red_merge.replace(dict,inplace = True) 


'''If any missing value occurs in the "Stress-induced_vascular_response_index(sVRI)", 
then Handing this.
'''
# Any missing values?
print(df_PPG_45_feat_1070_Red_merge.isnull().values.any())

# Total missing values for each feature
print(df_PPG_45_feat_1070_Red_merge.isnull().sum())

# Looking at the Stress-induced_vascular_response_index(sVRI) column
print(df_PPG_45_feat_1070_Red_merge['Stress-induced_vascular_response_index(sVRI)'].isnull())

# Replace using median 
median = df_PPG_45_feat_1070_Red_merge['Stress-induced_vascular_response_index(sVRI)'].median()
print(median)

# Replace using mean 
mean = df_PPG_45_feat_1070_Red_merge['Stress-induced_vascular_response_index(sVRI)'].mean()
print(mean)

#Find index of all rows with null values in a particular column in pandas dataframe
miss_lst = df_PPG_45_feat_1070_Red_merge[df_PPG_45_feat_1070_Red_merge['Stress-induced_vascular_response_index(sVRI)'].isnull()].index.tolist()
print(miss_lst)

# Location based replacement: replace with mean
df_PPG_45_feat_1070_Red_merge.loc[miss_lst,'Stress-induced_vascular_response_index(sVRI)'] = mean

# Total missing values for each feature
print(df_PPG_45_feat_1070_Red_merge.isnull().sum())


''' Create Finale Dataset '''
df_PPG_45_feat_1070_Red_merge.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Final_dataset/Pre_df_PPG_45_feat_1070_Red_merge.csv")

#
#''' Eliminate the wrong PPG cycle. '''
#df_1070_hemo = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_1070_Red_Hemo.csv")
#df_1070_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_1070_Red_Gl.csv")
#df_1070_hemo_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_1070_Red_merge.csv")
#
### Load the text file where put the wrong ID
#with open('/media/rezwan/Softwares/THESIS/PPG_Img/LED-1070.txt', 'r') as f:
#    lines = f.readlines()
#    
#junk_list_1070_red = []
#for e in lines:
#    for n in e.split(","):
#        junk_list_1070_red.append(int(n))
#    
#print(len(junk_list_1070_red))
#
#
### Eliminate the Id from the dataframe that indicates junk_list_0850_red
###### Hemoglobin
#print(df_1070_hemo.describe())
#print(df_1070_hemo.shape)
#print(df_1070_hemo['ID'])
#print(len(df_1070_hemo['ID']))
#
#df_1070_hemo_new = df_1070_hemo[~df_1070_hemo['ID'].isin(junk_list_1070_red)]
#
#print(df_1070_hemo_new.describe())
#print(df_1070_hemo_new.shape)
#print(df_1070_hemo_new['ID'])
#print(len(df_1070_hemo_new['ID']))
#
#df_1070_hemo_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Final_dataset/df_1070_hemo.csv")
#
#
###### Glucose
#print(df_1070_gl.describe())
#print(df_1070_gl.shape)
#print(df_1070_gl['ID'])
#print(len(df_1070_gl['ID']))
#
#df_1070_gl_new = df_1070_gl[~df_1070_gl['ID'].isin(junk_list_1070_red)]
#
#print(df_1070_gl_new.describe())
#print(df_1070_gl_new.shape)
#print(df_1070_gl_new['ID'])
#print(len(df_1070_gl_new['ID']))
#
#df_1070_gl_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Final_dataset/df_1070_gl.csv")
#
#
###### Hemo and Glucose
#print(df_1070_hemo_gl.describe())
#print(df_1070_hemo_gl.shape)
#print(df_1070_hemo_gl['ID'])
#print(len(df_1070_hemo_gl['ID']))
#
#df_1070_hemo_gl_new = df_1070_hemo_gl[~df_1070_hemo_gl['ID'].isin(junk_list_1070_red)]
#
#print(df_1070_hemo_gl_new.describe())
#print(df_1070_hemo_gl_new.shape)
#print(df_1070_hemo_gl_new['ID'])
#print(len(df_1070_hemo_gl_new['ID']))
#
#df_1070_hemo_gl_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/Final_dataset/df_1070_hemo_gl.csv")



                    ######################################
                    #          /LED-NO                #
                    #####################################
                    

LED_Broad_num = ["/LED-NO"]

PPG_45_feat_NO_Red = []

for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    print("Path : " + str(path))
    
    idx = 0 ##################
    
    for folder in folList:
        print("Folder : " + str(folder))
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
#        print(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                # print("Image size: " + str(img.shape))
                
                '''
                 #to get a square centered in the middle
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1]//2 - 250 # width//2 - 250. where width = 1920 px
                img = img[h:h+500, w:w+500]
                # img = img[290:790, 710:1210]
                '''
                
                #to get a square right to left
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1] # width. where width = 1920 px
                img = img[h:h+500, w-500:w]
                
                # print("Image size: " + str(img.shape))
                
                #print(file[:-4])
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                # print("Image size: " + str(img.shape))
                
                '''
                 #to get a square centered in the middle
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1]//2 - 250 # width//2 - 250. where width = 1920 px
                img = img[h:h+500, w:w+500]
                # img = img[290:790, 710:1210]
                '''
                
                #to get a square right to left
                h= img.shape[0]//2 - 250 # height//2 - 250. where height = 1080 px
                w= img.shape[1] # width. where width = 1920 px
                img = img[h:h+500, w-500:w]
                
                # print("Image size: " + str(img.shape))
                
                #print(file[:-4])
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
                
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
        #############################

        ##################################################
        #           R channel
        ##################################################
        # plot_time_series(r_mean, "r", "R channel")
        R_bandPass = bandPass(r_mean)
        R_bandPass = R_bandPass[::-1]
#        plot_time_series(R_bandPass, "r", "BandPass For R channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = R_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
#        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-NO/Red_img/"+str(ID)+"_"+str(LED_Broad)+"_red.png", dpi = 100)
        plt.show()
        
        ''' Pick Fresh 3 PPG wave '''
        from pyexcel_ods import get_data
        data = get_data("/media/rezwan/Softwares/THESIS/PPG_Img/LED-NO_RED.ods")
#        print(data)
        
        Red_2d_list = list(data.values())[0][1:]
        #print(Red_2d_list)
        
        Id = Red_2d_list[idx][0]
#        print(Id)
        idx_Red_2d_list = Red_2d_list[idx]
#        print(idx_Red_2d_list)
        
        Red_PPG_Clean = [] 
        for i in range(1, len(idx_Red_2d_list)-1, 2):
#            print(idx_Red_2d_list[i], idx_Red_2d_list[i+1])
            Red_PPG_Clean.append(list(series[idx_Red_2d_list[i]:idx_Red_2d_list[i+1]+1]))
        
#        print(list(Red_PPG_Clean))
        Red_PPG_Clean = sum(Red_PPG_Clean, [])
        plot_time_series_n_save_NO(Red_PPG_Clean, 'k', "Cleaned PPG Wave", Id, LED_Broad)
        idx += 1
        
        """ PPG-45 + extract_svri Feature Extraction """
        ppg_feat_45 = extract_ppg45(Red_PPG_Clean)
        svri = extract_svri(Red_PPG_Clean)
        
        ppg_feat_45.insert(0, int(ID))
        ppg_feat_45.append(svri)
        PPG_45_feat_NO_Red.append(ppg_feat_45)
        
"""     
        
        ##################################################
        #           Green channel
        ##################################################
        # plot_time_series(g_mean, "g", "G channel")
        G_bandPass = bandPass(g_mean)
        G_bandPass = G_bandPass[::-1]
#        plot_time_series(G_bandPass, "g", "BandPass For G channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = G_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-NO/Green_img/"+str(ID)+"_green.png", dpi = 100)
        plt.show()
        
        
        ##################################################
        #           Blue channel
        ##################################################
        # plot_time_series(b_mean, "b", "B channel")
        B_bandPass = bandPass(b_mean)
        B_bandPass = B_bandPass[::-1]
#        plot_time_series(B_bandPass, "b", "BandPass For B channel")
        
        '''Peak detection from PPG'''
        from matplotlib.pyplot import plot, scatter, show
        from numpy import array
        series = B_bandPass # [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
        maxtab, mintab = peakdet(series,0.1)
        
                
        pos_maxtab = array(maxtab)[:,0]
        val_maxtab = array(maxtab)[:,1]
        pos_mintab = array(mintab)[:,0]
        val_mintab = array(mintab)[:,1]
        
        plt.style.use('ggplot') # nicer plots
        np.random.seed(52102) 
        plt.figure(figsize=(22,14))
        plot(series)

        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='green')
        for i, txt in enumerate(pos_maxtab):
            plt.annotate(txt, (pos_maxtab[i], val_maxtab[i]))
            
        for i, txt in enumerate(pos_mintab):
            plt.annotate(txt, (pos_mintab[i], val_mintab[i]))
        
        plt.savefig("/media/rezwan/Softwares/THESIS/PPG_Img/LED-NO/Blue_img/"+str(ID)+"_blue.png", dpi = 100)
        plt.show()

"""

""" 
Preprocess the dataset for LED-NO_red
"""
headers = ['ID', 'Systolic_peak(x)', 'Diastolic_peak(y)', 'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)', 'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x', 'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)', 'Dicrotic_notch_time(t3)', 'Time_between_systolic_and_diastolic_peaks(∆T)', 'Time_between_half_systolic_peak_points(w)', 'Inflection_point_area_ratio(A2/A1)', 'Systolic_peak_rising_slope(t1/x)', 'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi', 't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2', '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi', 'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi', '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)', 'Fundamental_component_magnitude(|sbase|)', '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)', '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)', 'Stress-induced_vascular_response_index(sVRI)']       

df = pd.DataFrame(PPG_45_feat_NO_Red, columns=headers)
df.to_csv('CSV_Files/LED-NO_PPG_CSV/PPG_45_feat_NO_Red.csv')      

''' Merge PPG_45_feat_NO_Red and Clinical data '''
import numpy as np
import pandas as pd
df_Clinical = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Shared AmaderGram Data 2018-DataSheet - Clinical data.csv")

df_PPG_45_feat_NO_Red = pd.read_csv('CSV_Files/LED-NO_PPG_CSV/PPG_45_feat_NO_Red.csv')

df_PPG_45_feat_NO_Red_merge = pd.merge(df_PPG_45_feat_NO_Red, df_Clinical, on='ID', how='outer')

col_list = ['ID', 'Age', 'Sex', 'Systolic_peak(x)', 'Diastolic_peak(y)',
       'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)',
       'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x',
       'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)',
       'Dicrotic_notch_time(t3)',
       'Time_between_systolic_and_diastolic_peaks(∆T)',
       'Time_between_half_systolic_peak_points(w)',
       'Inflection_point_area_ratio(A2/A1)',
       'Systolic_peak_rising_slope(t1/x)',
       'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi',
       't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2',
       '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi',
       'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi',
       '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)',
       'Fundamental_component_magnitude(|sbase|)',
       '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)',
       '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)',
       'Stress-induced_vascular_response_index(sVRI)',  'Hb (gm/dL)']
print(len(col_list))

df_PPG_45_feat_NO_Red_merge = df_PPG_45_feat_NO_Red_merge[col_list]

dict = {'Sex':{'Male':1, 'Female':0}}      # label = column name
df_PPG_45_feat_NO_Red_merge.replace(dict,inplace = True) 

'''If any missing value occurs in the "Stress-induced_vascular_response_index(sVRI)", 
then Handing this.
'''
# Any missing values?
print(df_PPG_45_feat_NO_Red_merge.isnull().values.any())

# Total missing values for each feature
print(df_PPG_45_feat_NO_Red_merge.isnull().sum())

# Looking at the Stress-induced_vascular_response_index(sVRI) column
print(df_PPG_45_feat_NO_Red_merge['Stress-induced_vascular_response_index(sVRI)'].isnull())

# Replace using median 
median = df_PPG_45_feat_NO_Red_merge['Stress-induced_vascular_response_index(sVRI)'].median()
print(median)

# Replace using mean 
mean = df_PPG_45_feat_NO_Red_merge['Stress-induced_vascular_response_index(sVRI)'].mean()
print(mean)

#Find index of all rows with null values in a particular column in pandas dataframe
miss_lst = df_PPG_45_feat_NO_Red_merge[df_PPG_45_feat_NO_Red_merge['Stress-induced_vascular_response_index(sVRI)'].isnull()].index.tolist()
print(miss_lst)

# Location based replacement: replace with mean
df_PPG_45_feat_NO_Red_merge.loc[miss_lst,'Stress-induced_vascular_response_index(sVRI)'] = mean

# Total missing values for each feature
print(df_PPG_45_feat_NO_Red_merge.isnull().sum())

''' Create finale dataset '''
df_PPG_45_feat_NO_Red_merge.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Final_dataset/Pre_df_PPG_45_feat_NO_Red_merge.csv")

#
#''' Eliminate the wrong PPG cycle. '''
#df_NO_hemo = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_NO_Red_Hemo.csv")
#df_NO_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_NO_Red_Gl.csv")
#df_NO_hemo_gl = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Preprocess_Dataset/Pre_df_PPG_45_feat_NO_Red_merge.csv")
#
### Load the text file where put the wrong ID
#with open('/media/rezwan/Softwares/THESIS/PPG_Img/LED-NO.txt', 'r') as f:
#    lines = f.readlines()
#    
#junk_list_NO_red = []
#for e in lines:
#    for n in e.split(","):
#        junk_list_NO_red.append(int(n))
#    
#print(len(junk_list_NO_red))
#
#
### Eliminate the Id from the dataframe that indicates junk_list_0850_red
###### Hemoglobin
#print(df_NO_hemo.describe())
#print(df_NO_hemo.shape)
#print(df_NO_hemo['ID'])
#print(len(df_NO_hemo['ID']))
#
#df_NO_hemo_new = df_NO_hemo[~df_NO_hemo['ID'].isin(junk_list_NO_red)]
#
#print(df_NO_hemo_new.describe())
#print(df_NO_hemo_new.shape)
#print(df_NO_hemo_new['ID'])
#print(len(df_NO_hemo_new['ID']))
#
#df_NO_hemo_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Final_dataset/df_NO_hemo.csv")
#
#
###### Glucose
#print(df_NO_gl.describe())
#print(df_NO_gl.shape)
#print(df_NO_gl['ID'])
#print(len(df_NO_gl['ID']))
#
#df_NO_gl_new = df_NO_gl[~df_NO_gl['ID'].isin(junk_list_NO_red)]
#
#print(df_NO_gl_new.describe())
#print(df_NO_gl_new.shape)
#print(df_NO_gl_new['ID'])
#print(len(df_NO_gl_new['ID']))
#
#df_NO_gl_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Final_dataset/df_NO_gl.csv")
#
#
###### Hemo and Glucose
#print(df_NO_hemo_gl.describe())
#print(df_NO_hemo_gl.shape)
#print(df_NO_hemo_gl['ID'])
#print(len(df_NO_hemo_gl['ID']))
#
#df_NO_hemo_gl_new = df_NO_hemo_gl[~df_NO_hemo_gl['ID'].isin(junk_list_NO_red)]
#
#print(df_NO_hemo_gl_new.describe())
#print(df_NO_hemo_gl_new.shape)
#print(df_NO_hemo_gl_new['ID'])
#print(len(df_NO_hemo_gl_new['ID']))
#
#df_NO_hemo_gl_new.to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/Final_dataset/df_NO_hemo_gl.csv")


      
###################################################

#        
#        values, posns = max_first_three(val_maxtab)
##        print(values, posns)
#        
#        rez = []
#        
#        for idx in posns:
#            left_idx = idx - 1
#            right_idx = idx #+ 1
#            if left_idx < 0:
#                left_idx = left_idx + 1
#            elif right_idx >= len(pos_mintab):
#                right_idx = right_idx - 1 
#                
#            #print(pos_mintab[left_idx], pos_mintab[right_idx])
#            l_idx = int(pos_mintab[left_idx])
#            r_idx = int(pos_mintab[right_idx]) + 1
##            print(series[l_idx:r_idx])
#            rez.append(list(series[l_idx:r_idx]))
#            
##        print(list(rez))
#        ppg_lst = sum(rez, []) 
##        print(ppg_lst)
#        plot_time_series(ppg_lst, "r", "Filter PPG")
#  
  
