#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 19:05:41 2018

@author: rezwan
"""

##################################################################
        
from libraries import *
from plot_peak_frq import *
from plot_time_series import *

'''
Apply Deep Learing
'''

##################################################################################
'''                R Channel , G Channel, B Channel  For LED-0850    '''
##################################################################################



""" Generate .csv files for  R, G, B channels respectively . """

#### Select the directory
#src_dir = "Extract-Images"  
##src_dir = "RawData/Extract-Images"
#
#LED_Broad_num = ["/LED-0850"]
##LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"] #, "/LED-0940", "/LED-1070","/LED-NO"

# directory
img_dir = "/media/rezwan/Softwares/THESIS/Extract-Images/LED-0850"

#/////////////////////////////
#No of classes in datasets (names)

img_classes=["1007-0850-VID_20180625_145037=9.9","1011-0850-VID_20180626_132129=12.1","1025-0850-VID_20180701_135904=13.0"]



#///////////////////////////////////////////////////
# resize image
# ////////////////////////////////////////////////
#////////////////////////////////

img_size=80
'''
new_img_array=cv2.resize(img_array,(img_size,img_size))
plt.imshow(new_img_array,cmap='gray')
plt.show()
'''
#////////////////////////////////////////////////////
# now create training data
from tqdm import tqdm
training_data=[]

def create_training_data():
    for clas in tqdm(img_classes):
            path=os.path.join(img_dir,clas)
            #index no start from 0 1 2 3
            print("Path: " + str(path))
            hemo_cls = path.split('/')[-1].split('=')[1]
            
            hemo_cls = float(hemo_cls)
            
#            print(clas)
            
#            class_num=img_classes.index(clas)
#            print("Class Num: " + str(class_num))
            
            for img in os.listdir(path):
                try:
                    img_array=cv2.imread(os.path.join(path,img),cv2.IMREAD_GRAYSCALE)
                    new_array=cv2.resize(img_array,(img_size,img_size))
                    training_data.append([new_array,hemo_cls])
                except Exception as e:
                    pass
    
       
create_training_data()
print(len(training_data))



#///////////////////////////////////////////////////

xs=[]
ys=[]

for features,labels in tqdm(training_data):
    xs.append(features)
    ys.append(labels)

xs=np.array(xs).reshape(-1,img_size,img_size,1)
ys=np.array(ys).reshape(-1,1)
import pickle

pickle_out=open("xs.pickle","wb")
pickle.dump(xs,pickle_out)
pickle_out.close()



pickle_out=open("ys.pickle","wb")
pickle.dump(ys,pickle_out)
pickle_out.close()

