#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  6 14:23:11 2018

@author: rezwan
"""

##################################################################
        
from libraries import *
from plot_peak_frq import *
from plot_time_series import *
from Statistics import *

'''
Statics Analysis on data
'''

##################################################################################
'''                R Channel , G Channel, B Channel  For LED-0850    '''
##################################################################################

""" Generate .csv files for  R, G, B channels respectively . """

### Select the directory
#src_dir = "Extract-Images"  
src_dir = "RawData/Extract-Images"


##/////////////////////////////////////////////
##                   LED-0850
##////////////////////////////////////////////

LED_Broad_num = ["/LED-0850"]
#LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"] #, "/LED-0940", "/LED-1070","/LED-NO"

Sub_ID = []

#===========================Dominant 3 energy=====================================
wave_energy_1st_R = []
wave_energy_1st_G = []
wave_energy_1st_B = []

wave_energy_2nd_R = []
wave_energy_2nd_G = []
wave_energy_2nd_B = []

wave_energy_3rd_R = []
wave_energy_3rd_G = []
wave_energy_3rd_B = []

#===========================Dominant 3 one_sided_spectrum_magnitude ==========
one_sided_spectrum_magnitude_1st_R = []
one_sided_spectrum_magnitude_1st_G = []
one_sided_spectrum_magnitude_1st_B = []

one_sided_spectrum_magnitude_2nd_R = []
one_sided_spectrum_magnitude_2nd_G = []
one_sided_spectrum_magnitude_2nd_B = []

one_sided_spectrum_magnitude_3rd_R = []
one_sided_spectrum_magnitude_3rd_G = []
one_sided_spectrum_magnitude_3rd_B = []

#========================= Statistics Analysis =============================
Mean_R = []
Mean_G = []
Mean_B = []

Variance_R = []
Variance_G = []
Variance_B = []

pVariance_R = []
pVariance_G = []
pVariance_B = []

Skewness_R = []
Skewness_G = []
Skewness_B = []

Kurtosis_R = []
Kurtosis_G = []
Kurtosis_B = []

stdev_R = []
stdev_G = []
stdev_B = []

pstdev_R = []
pstdev_G = []
pstdev_B = []


median_R = []
median_G = []
median_B = []

median_low_R = []
median_low_G = []
median_low_B = []

median_high_R = []
median_high_G = []
median_high_B = []

median_grouped_R = []
median_grouped_G = []
median_grouped_B = []

harmonic_mean_R = []
harmonic_mean_G = []
harmonic_mean_B = []

Max_R = []
Max_G = []
Max_B = []

Min_R = []
Min_G = []
Min_B = []

shannon_entropy_R = []
shannon_entropy_G = []
shannon_entropy_B = []

hartley_func_R = []
hartley_func_G = []
hartley_func_B = []

percentile_50th_R = []
percentile_50th_G = []
percentile_50th_B = []


for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    
    for folder in folList:
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
            

        
        
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
    
        Sub_ID.append(int(ID))
        
        #############################
        #    R channel             #            
        #############################
        print("==============For R channel:===================")
#        plot_time_series(r_mean, "r", "R Channnel")
        
        ################## Dominant 3 frequency(Energy and magnitude) #############################
        '''R channel'''
        top_3_waves = Dominant_3_Waves(r_mean) # R channel
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_R.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_R.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_R.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_R.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_R.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_R.append(one_sided_spectrum_magnitude_3)
        
        '''G channel'''
        top_3_waves = Dominant_3_Waves(g_mean) # G channel 
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_G.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_G.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_G.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_G.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_G.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_G.append(one_sided_spectrum_magnitude_3)
        
        '''B channel'''
        top_3_waves = Dominant_3_Waves(b_mean) # B channel 
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_B.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_B.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_B.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_B.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_B.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_B.append(one_sided_spectrum_magnitude_3)
        
        #========================= Statistics Analysis =============================
        Mean = mean(r_mean)
        Mean_R.append(Mean)
        Mean = mean(g_mean)
        Mean_G.append(Mean)
        Mean = mean(b_mean)
        Mean_B.append(Mean)
        
        Var = variance(r_mean)
        Variance_R.append(Var)
        Var = variance(g_mean)
        Variance_G.append(Var)
        Var = variance(b_mean)
        Variance_B.append(Var)
        
        pVar = pvariance(r_mean)
        pVariance_R.append(pVar)
        pVar = pvariance(g_mean)
        pVariance_G.append(pVar)
        pVar = pvariance(b_mean)
        pVariance_B.append(pVar)
        
        Skew = skewness(r_mean)
        Skewness_R.append(Skew)
        Skew = skewness(g_mean)
        Skewness_G.append(Skew)
        Skew = skewness(b_mean)
        Skewness_B.append(Skew)
        
        Kur = kurtosis(r_mean)
        Kurtosis_R.append(Kur)
        Kur = kurtosis(g_mean)
        Kurtosis_G.append(Kur)
        Kur = kurtosis(b_mean)
        Kurtosis_B.append(Kur)
        
        std = stdev(r_mean)
        stdev_R.append(std)
        std = stdev(g_mean)
        stdev_G.append(std)
        std = stdev(b_mean)
        stdev_B.append(std)
        
        pstd = pstdev(r_mean)
        pstdev_R.append(pstd)
        pstd = pstdev(g_mean)
        pstdev_G.append(pstd)
        pstd = pstdev(b_mean)
        pstdev_B.append(pstd)
        
        Median = median(r_mean)
        median_R.append(Median)
        Median = median(g_mean)
        median_G.append(Median)
        Median = median(b_mean)
        median_B.append(Median)
        
        Med_low = median_low(r_mean)
        median_low_R.append(Med_low)
        Med_low = median_low(g_mean)
        median_low_G.append(Med_low)
        Med_low = median_low(b_mean)
        median_low_B.append(Med_low)
        
        Med_high = median_high(r_mean)
        median_high_R.append(Med_high)
        Med_high = median_high(g_mean)
        median_high_G.append(Med_high)
        Med_high = median_high(b_mean)
        median_high_B.append(Med_high)
        
        Med_grp = median_grouped(r_mean)
        median_grouped_R.append(Med_grp)
        Med_grp = median_grouped(g_mean)
        median_grouped_G.append(Med_grp)
        Med_grp = median_grouped(b_mean)
        median_grouped_B.append(Med_grp)
        
        har_mean = harmonic_mean(r_mean)
        harmonic_mean_R.append(har_mean)
        har_mean = harmonic_mean(g_mean)
        harmonic_mean_G.append(har_mean)
        har_mean = harmonic_mean(b_mean)
        harmonic_mean_B.append(har_mean)
        
        Mx = Max(r_mean)
        Max_R.append(Mx)
        Mx = Max(g_mean)
        Max_G.append(Mx)
        Mx = Max(b_mean)
        Max_B.append(Mx)
        
        Mn = Min(r_mean)
        Min_R.append(Mn)
        Mn = Min(g_mean)
        Min_G.append(Mn)
        Mn = Min(b_mean)
        Min_B.append(Mn)
        
        shan = shannon_entropy(r_mean)
        shannon_entropy_R.append(shan)
        shan = shannon_entropy(g_mean)
        shannon_entropy_G.append(shan)
        shan = shannon_entropy(b_mean)
        shannon_entropy_B.append(shan)
        
        hart = hartley_func(r_mean)
        hartley_func_R.append(hart)
        hart = hartley_func(g_mean)
        hartley_func_G.append(hart)
        hart = hartley_func(b_mean)
        hartley_func_B.append(hart)
        
        per = percentile_50th(r_mean)
        percentile_50th_R.append(per)
        per = percentile_50th(g_mean)
        percentile_50th_G.append(per)
        per = percentile_50th(b_mean)
        percentile_50th_B.append(per)
        
        


'''Create the DataFrame for R channel'''
dataFrame_R_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_R": wave_energy_1st_R, 
    "wave_energy_2nd_R": wave_energy_2nd_R,
    "wave_energy_3rd_R": wave_energy_3rd_R,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_R": one_sided_spectrum_magnitude_1st_R,
    "one_sided_spectrum_magnitude_2nd_R": one_sided_spectrum_magnitude_2nd_R,
    "one_sided_spectrum_magnitude_3rd_R": one_sided_spectrum_magnitude_3rd_R,
    #========================= Statistics Analysis =============================
    "Mean_R": Mean_R,
    
    "Variance_R": Variance_R,
    
    "pVariance_R": pVariance_R,

    "Skewness_R": Skewness_R,

    "Kurtosis_R": Kurtosis_R,

    "stdev_R": stdev_R,
    
    "pstdev_R": pstdev_R,
    
    "median_R": median_R,
    
    "median_low_R": median_low_R,
    
    "median_high_R": median_high_R,
    
    "median_grouped_R": median_grouped_R,
    
    "harmonic_mean_R": harmonic_mean_R,
    
    "Max_R": Max_R,
    
    "Min_R": Min_R,
    
    "shannon_entropy_R": shannon_entropy_R, 
    
    "hartley_func_R": hartley_func_R,
    
    "percentile_50th_R": percentile_50th_R

})
    
dataFrame_R_channel.to_csv('CSV_Files/Statistics_Data/R_Statistics_LED-0850.csv')

'''Create the DataFrame for G channel'''
dataFrame_G_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_G": wave_energy_1st_G, 
    "wave_energy_2nd_G": wave_energy_2nd_G,
    "wave_energy_3rd_G": wave_energy_3rd_G,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_G": one_sided_spectrum_magnitude_1st_G,
    "one_sided_spectrum_magnitude_2nd_G": one_sided_spectrum_magnitude_2nd_G,
    "one_sided_spectrum_magnitude_3rd_G": one_sided_spectrum_magnitude_3rd_G,
    #========================= Statistics Analysis =============================
    "Mean_G": Mean_G,
    
    "Variance_G": Variance_G,
    
    "pVariance_G": pVariance_G,

    "Skewness_G": Skewness_G,

    "Kurtosis_G": Kurtosis_G,

    "stdev_G": stdev_G,
    
    "pstdev_G": pstdev_G,
    
    "median_G": median_G,
    
    "median_low_G": median_low_G,
    
    "median_high_G": median_high_G,
    
    "median_grouped_G": median_grouped_G,
    
    "harmonic_mean_G": harmonic_mean_G,
    
    "Max_G": Max_G,
    
    "Min_G": Min_G,
    
    "shannon_entropy_G": shannon_entropy_G, 
    
    "hartley_func_G": hartley_func_G,
    
    "percentile_50th_G": percentile_50th_G

})
    
dataFrame_G_channel.to_csv('CSV_Files/Statistics_Data/G_Statistics_LED-0850.csv')

'''Create the DataFrame for B channel'''
dataFrame_B_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_B": wave_energy_1st_B, 
    "wave_energy_2nd_B": wave_energy_2nd_B,
    "wave_energy_3rd_B": wave_energy_3rd_B,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_B": one_sided_spectrum_magnitude_1st_B,
    "one_sided_spectrum_magnitude_2nd_B": one_sided_spectrum_magnitude_2nd_B,
    "one_sided_spectrum_magnitude_3rd_B": one_sided_spectrum_magnitude_3rd_B,
    #========================= Statistics Analysis =============================
    "Mean_B": Mean_B,
    
    "Variance_B": Variance_B,
    
    "pVariance_B": pVariance_B,

    "Skewness_B": Skewness_B,

    "Kurtosis_B": Kurtosis_B,

    "stdev_B": stdev_B,
    
    "pstdev_B": pstdev_B,
    
    "median_B": median_B,
    
    "median_low_B": median_low_B,
    
    "median_high_B": median_high_B,
    
    "median_grouped_B": median_grouped_B,
    
    "harmonic_mean_B": harmonic_mean_B,
    
    "Max_B": Max_B,
    
    "Min_B": Min_B,
    
    "shannon_entropy_B": shannon_entropy_B, 
    
    "hartley_func_B": hartley_func_B,
    
    "percentile_50th_B": percentile_50th_B

})
    
dataFrame_B_channel.to_csv('CSV_Files/Statistics_Data/B_Statistics_LED-0850.csv')




##/////////////////////////////////////////////
##                   LED-0940
##////////////////////////////////////////////

LED_Broad_num = ["/LED-0940"]
#LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"] #, "/LED-0940", "/LED-1070","/LED-NO"

Sub_ID = []

#===========================Dominant 3 energy=====================================
wave_energy_1st_R = []
wave_energy_1st_G = []
wave_energy_1st_B = []

wave_energy_2nd_R = []
wave_energy_2nd_G = []
wave_energy_2nd_B = []

wave_energy_3rd_R = []
wave_energy_3rd_G = []
wave_energy_3rd_B = []

#===========================Dominant 3 one_sided_spectrum_magnitude ==========
one_sided_spectrum_magnitude_1st_R = []
one_sided_spectrum_magnitude_1st_G = []
one_sided_spectrum_magnitude_1st_B = []

one_sided_spectrum_magnitude_2nd_R = []
one_sided_spectrum_magnitude_2nd_G = []
one_sided_spectrum_magnitude_2nd_B = []

one_sided_spectrum_magnitude_3rd_R = []
one_sided_spectrum_magnitude_3rd_G = []
one_sided_spectrum_magnitude_3rd_B = []

#========================= Statistics Analysis =============================
Mean_R = []
Mean_G = []
Mean_B = []

Variance_R = []
Variance_G = []
Variance_B = []

pVariance_R = []
pVariance_G = []
pVariance_B = []

Skewness_R = []
Skewness_G = []
Skewness_B = []

Kurtosis_R = []
Kurtosis_G = []
Kurtosis_B = []

stdev_R = []
stdev_G = []
stdev_B = []

pstdev_R = []
pstdev_G = []
pstdev_B = []


median_R = []
median_G = []
median_B = []

median_low_R = []
median_low_G = []
median_low_B = []

median_high_R = []
median_high_G = []
median_high_B = []

median_grouped_R = []
median_grouped_G = []
median_grouped_B = []

harmonic_mean_R = []
harmonic_mean_G = []
harmonic_mean_B = []

Max_R = []
Max_G = []
Max_B = []

Min_R = []
Min_G = []
Min_B = []

shannon_entropy_R = []
shannon_entropy_G = []
shannon_entropy_B = []

hartley_func_R = []
hartley_func_G = []
hartley_func_B = []

percentile_50th_R = []
percentile_50th_G = []
percentile_50th_B = []


for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    
    for folder in folList:
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
            

        
        
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
    
        Sub_ID.append(int(ID))
        
        #############################
        #    R channel             #            
        #############################
        print("==============For R channel:===================")
#        plot_time_series(r_mean, "r", "R Channnel")
        
        ################## Dominant 3 frequency(Energy and magnitude) #############################
        '''R channel'''
        top_3_waves = Dominant_3_Waves(r_mean) # R channel
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_R.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_R.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_R.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_R.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_R.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_R.append(one_sided_spectrum_magnitude_3)
        
        '''G channel'''
        top_3_waves = Dominant_3_Waves(g_mean) # G channel 
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_G.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_G.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_G.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_G.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_G.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_G.append(one_sided_spectrum_magnitude_3)
        
        '''B channel'''
        top_3_waves = Dominant_3_Waves(b_mean) # B channel 
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_B.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_B.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_B.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_B.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_B.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_B.append(one_sided_spectrum_magnitude_3)
        
        #========================= Statistics Analysis =============================
        Mean = mean(r_mean)
        Mean_R.append(Mean)
        Mean = mean(g_mean)
        Mean_G.append(Mean)
        Mean = mean(b_mean)
        Mean_B.append(Mean)
        
        Var = variance(r_mean)
        Variance_R.append(Var)
        Var = variance(g_mean)
        Variance_G.append(Var)
        Var = variance(b_mean)
        Variance_B.append(Var)
        
        pVar = pvariance(r_mean)
        pVariance_R.append(pVar)
        pVar = pvariance(g_mean)
        pVariance_G.append(pVar)
        pVar = pvariance(b_mean)
        pVariance_B.append(pVar)
        
        Skew = skewness(r_mean)
        Skewness_R.append(Skew)
        Skew = skewness(g_mean)
        Skewness_G.append(Skew)
        Skew = skewness(b_mean)
        Skewness_B.append(Skew)
        
        Kur = kurtosis(r_mean)
        Kurtosis_R.append(Kur)
        Kur = kurtosis(g_mean)
        Kurtosis_G.append(Kur)
        Kur = kurtosis(b_mean)
        Kurtosis_B.append(Kur)
        
        std = stdev(r_mean)
        stdev_R.append(std)
        std = stdev(g_mean)
        stdev_G.append(std)
        std = stdev(b_mean)
        stdev_B.append(std)
        
        pstd = pstdev(r_mean)
        pstdev_R.append(pstd)
        pstd = pstdev(g_mean)
        pstdev_G.append(pstd)
        pstd = pstdev(b_mean)
        pstdev_B.append(pstd)
        
        Median = median(r_mean)
        median_R.append(Median)
        Median = median(g_mean)
        median_G.append(Median)
        Median = median(b_mean)
        median_B.append(Median)
        
        Med_low = median_low(r_mean)
        median_low_R.append(Med_low)
        Med_low = median_low(g_mean)
        median_low_G.append(Med_low)
        Med_low = median_low(b_mean)
        median_low_B.append(Med_low)
        
        Med_high = median_high(r_mean)
        median_high_R.append(Med_high)
        Med_high = median_high(g_mean)
        median_high_G.append(Med_high)
        Med_high = median_high(b_mean)
        median_high_B.append(Med_high)
        
        Med_grp = median_grouped(r_mean)
        median_grouped_R.append(Med_grp)
        Med_grp = median_grouped(g_mean)
        median_grouped_G.append(Med_grp)
        Med_grp = median_grouped(b_mean)
        median_grouped_B.append(Med_grp)
        
        har_mean = harmonic_mean(r_mean)
        harmonic_mean_R.append(har_mean)
        har_mean = harmonic_mean(g_mean)
        harmonic_mean_G.append(har_mean)
        har_mean = harmonic_mean(b_mean)
        harmonic_mean_B.append(har_mean)
        
        Mx = Max(r_mean)
        Max_R.append(Mx)
        Mx = Max(g_mean)
        Max_G.append(Mx)
        Mx = Max(b_mean)
        Max_B.append(Mx)
        
        Mn = Min(r_mean)
        Min_R.append(Mn)
        Mn = Min(g_mean)
        Min_G.append(Mn)
        Mn = Min(b_mean)
        Min_B.append(Mn)
        
        shan = shannon_entropy(r_mean)
        shannon_entropy_R.append(shan)
        shan = shannon_entropy(g_mean)
        shannon_entropy_G.append(shan)
        shan = shannon_entropy(b_mean)
        shannon_entropy_B.append(shan)
        
        hart = hartley_func(r_mean)
        hartley_func_R.append(hart)
        hart = hartley_func(g_mean)
        hartley_func_G.append(hart)
        hart = hartley_func(b_mean)
        hartley_func_B.append(hart)
        
        per = percentile_50th(r_mean)
        percentile_50th_R.append(per)
        per = percentile_50th(g_mean)
        percentile_50th_G.append(per)
        per = percentile_50th(b_mean)
        percentile_50th_B.append(per)
        
        


'''Create the DataFrame for R channel'''
dataFrame_R_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_R": wave_energy_1st_R, 
    "wave_energy_2nd_R": wave_energy_2nd_R,
    "wave_energy_3rd_R": wave_energy_3rd_R,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_R": one_sided_spectrum_magnitude_1st_R,
    "one_sided_spectrum_magnitude_2nd_R": one_sided_spectrum_magnitude_2nd_R,
    "one_sided_spectrum_magnitude_3rd_R": one_sided_spectrum_magnitude_3rd_R,
    #========================= Statistics Analysis =============================
    "Mean_R": Mean_R,
    
    "Variance_R": Variance_R,
    
    "pVariance_R": pVariance_R,

    "Skewness_R": Skewness_R,

    "Kurtosis_R": Kurtosis_R,

    "stdev_R": stdev_R,
    
    "pstdev_R": pstdev_R,
    
    "median_R": median_R,
    
    "median_low_R": median_low_R,
    
    "median_high_R": median_high_R,
    
    "median_grouped_R": median_grouped_R,
    
    "harmonic_mean_R": harmonic_mean_R,
    
    "Max_R": Max_R,
    
    "Min_R": Min_R,
    
    "shannon_entropy_R": shannon_entropy_R, 
    
    "hartley_func_R": hartley_func_R,
    
    "percentile_50th_R": percentile_50th_R

})
    
dataFrame_R_channel.to_csv('CSV_Files/Statistics_Data/R_Statistics_LED-0940.csv')

'''Create the DataFrame for G channel'''
dataFrame_G_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_G": wave_energy_1st_G, 
    "wave_energy_2nd_G": wave_energy_2nd_G,
    "wave_energy_3rd_G": wave_energy_3rd_G,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_G": one_sided_spectrum_magnitude_1st_G,
    "one_sided_spectrum_magnitude_2nd_G": one_sided_spectrum_magnitude_2nd_G,
    "one_sided_spectrum_magnitude_3rd_G": one_sided_spectrum_magnitude_3rd_G,
    #========================= Statistics Analysis =============================
    "Mean_G": Mean_G,
    
    "Variance_G": Variance_G,
    
    "pVariance_G": pVariance_G,

    "Skewness_G": Skewness_G,

    "Kurtosis_G": Kurtosis_G,

    "stdev_G": stdev_G,
    
    "pstdev_G": pstdev_G,
    
    "median_G": median_G,
    
    "median_low_G": median_low_G,
    
    "median_high_G": median_high_G,
    
    "median_grouped_G": median_grouped_G,
    
    "harmonic_mean_G": harmonic_mean_G,
    
    "Max_G": Max_G,
    
    "Min_G": Min_G,
    
    "shannon_entropy_G": shannon_entropy_G, 
    
    "hartley_func_G": hartley_func_G,
    
    "percentile_50th_G": percentile_50th_G

})
    
dataFrame_G_channel.to_csv('CSV_Files/Statistics_Data/G_Statistics_LED-0940.csv')

'''Create the DataFrame for B channel'''
dataFrame_B_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_B": wave_energy_1st_B, 
    "wave_energy_2nd_B": wave_energy_2nd_B,
    "wave_energy_3rd_B": wave_energy_3rd_B,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_B": one_sided_spectrum_magnitude_1st_B,
    "one_sided_spectrum_magnitude_2nd_B": one_sided_spectrum_magnitude_2nd_B,
    "one_sided_spectrum_magnitude_3rd_B": one_sided_spectrum_magnitude_3rd_B,
    #========================= Statistics Analysis =============================
    "Mean_B": Mean_B,
    
    "Variance_B": Variance_B,
    
    "pVariance_B": pVariance_B,

    "Skewness_B": Skewness_B,

    "Kurtosis_B": Kurtosis_B,

    "stdev_B": stdev_B,
    
    "pstdev_B": pstdev_B,
    
    "median_B": median_B,
    
    "median_low_B": median_low_B,
    
    "median_high_B": median_high_B,
    
    "median_grouped_B": median_grouped_B,
    
    "harmonic_mean_B": harmonic_mean_B,
    
    "Max_B": Max_B,
    
    "Min_B": Min_B,
    
    "shannon_entropy_B": shannon_entropy_B, 
    
    "hartley_func_B": hartley_func_B,
    
    "percentile_50th_B": percentile_50th_B

})
    
dataFrame_B_channel.to_csv('CSV_Files/Statistics_Data/B_Statistics_LED-0940.csv')



##/////////////////////////////////////////////
##                   LED-1070
##////////////////////////////////////////////

LED_Broad_num = ["/LED-1070"]
#LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"] #, "/LED-0940", "/LED-1070","/LED-NO"

Sub_ID = []

#===========================Dominant 3 energy=====================================
wave_energy_1st_R = []
wave_energy_1st_G = []
wave_energy_1st_B = []

wave_energy_2nd_R = []
wave_energy_2nd_G = []
wave_energy_2nd_B = []

wave_energy_3rd_R = []
wave_energy_3rd_G = []
wave_energy_3rd_B = []

#===========================Dominant 3 one_sided_spectrum_magnitude ==========
one_sided_spectrum_magnitude_1st_R = []
one_sided_spectrum_magnitude_1st_G = []
one_sided_spectrum_magnitude_1st_B = []

one_sided_spectrum_magnitude_2nd_R = []
one_sided_spectrum_magnitude_2nd_G = []
one_sided_spectrum_magnitude_2nd_B = []

one_sided_spectrum_magnitude_3rd_R = []
one_sided_spectrum_magnitude_3rd_G = []
one_sided_spectrum_magnitude_3rd_B = []

#========================= Statistics Analysis =============================
Mean_R = []
Mean_G = []
Mean_B = []

Variance_R = []
Variance_G = []
Variance_B = []

pVariance_R = []
pVariance_G = []
pVariance_B = []

Skewness_R = []
Skewness_G = []
Skewness_B = []

Kurtosis_R = []
Kurtosis_G = []
Kurtosis_B = []

stdev_R = []
stdev_G = []
stdev_B = []

pstdev_R = []
pstdev_G = []
pstdev_B = []


median_R = []
median_G = []
median_B = []

median_low_R = []
median_low_G = []
median_low_B = []

median_high_R = []
median_high_G = []
median_high_B = []

median_grouped_R = []
median_grouped_G = []
median_grouped_B = []

harmonic_mean_R = []
harmonic_mean_G = []
harmonic_mean_B = []

Max_R = []
Max_G = []
Max_B = []

Min_R = []
Min_G = []
Min_B = []

shannon_entropy_R = []
shannon_entropy_G = []
shannon_entropy_B = []

hartley_func_R = []
hartley_func_G = []
hartley_func_B = []

percentile_50th_R = []
percentile_50th_G = []
percentile_50th_B = []


for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    
    for folder in folList:
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
            

        
        
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
    
        Sub_ID.append(int(ID))
        
        #############################
        #    R channel             #            
        #############################
        print("==============For R channel:===================")
#        plot_time_series(r_mean, "r", "R Channnel")
        
        ################## Dominant 3 frequency(Energy and magnitude) #############################
        '''R channel'''
        top_3_waves = Dominant_3_Waves(r_mean) # R channel
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_R.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_R.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_R.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_R.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_R.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_R.append(one_sided_spectrum_magnitude_3)
        
        '''G channel'''
        top_3_waves = Dominant_3_Waves(g_mean) # G channel 
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_G.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_G.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_G.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_G.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_G.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_G.append(one_sided_spectrum_magnitude_3)
        
        '''B channel'''
        top_3_waves = Dominant_3_Waves(b_mean) # B channel 
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_B.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_B.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_B.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_B.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_B.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_B.append(one_sided_spectrum_magnitude_3)
        
        #========================= Statistics Analysis =============================
        Mean = mean(r_mean)
        Mean_R.append(Mean)
        Mean = mean(g_mean)
        Mean_G.append(Mean)
        Mean = mean(b_mean)
        Mean_B.append(Mean)
        
        Var = variance(r_mean)
        Variance_R.append(Var)
        Var = variance(g_mean)
        Variance_G.append(Var)
        Var = variance(b_mean)
        Variance_B.append(Var)
        
        pVar = pvariance(r_mean)
        pVariance_R.append(pVar)
        pVar = pvariance(g_mean)
        pVariance_G.append(pVar)
        pVar = pvariance(b_mean)
        pVariance_B.append(pVar)
        
        Skew = skewness(r_mean)
        Skewness_R.append(Skew)
        Skew = skewness(g_mean)
        Skewness_G.append(Skew)
        Skew = skewness(b_mean)
        Skewness_B.append(Skew)
        
        Kur = kurtosis(r_mean)
        Kurtosis_R.append(Kur)
        Kur = kurtosis(g_mean)
        Kurtosis_G.append(Kur)
        Kur = kurtosis(b_mean)
        Kurtosis_B.append(Kur)
        
        std = stdev(r_mean)
        stdev_R.append(std)
        std = stdev(g_mean)
        stdev_G.append(std)
        std = stdev(b_mean)
        stdev_B.append(std)
        
        pstd = pstdev(r_mean)
        pstdev_R.append(pstd)
        pstd = pstdev(g_mean)
        pstdev_G.append(pstd)
        pstd = pstdev(b_mean)
        pstdev_B.append(pstd)
        
        Median = median(r_mean)
        median_R.append(Median)
        Median = median(g_mean)
        median_G.append(Median)
        Median = median(b_mean)
        median_B.append(Median)
        
        Med_low = median_low(r_mean)
        median_low_R.append(Med_low)
        Med_low = median_low(g_mean)
        median_low_G.append(Med_low)
        Med_low = median_low(b_mean)
        median_low_B.append(Med_low)
        
        Med_high = median_high(r_mean)
        median_high_R.append(Med_high)
        Med_high = median_high(g_mean)
        median_high_G.append(Med_high)
        Med_high = median_high(b_mean)
        median_high_B.append(Med_high)
        
        Med_grp = median_grouped(r_mean)
        median_grouped_R.append(Med_grp)
        Med_grp = median_grouped(g_mean)
        median_grouped_G.append(Med_grp)
        Med_grp = median_grouped(b_mean)
        median_grouped_B.append(Med_grp)
        
        har_mean = harmonic_mean(r_mean)
        harmonic_mean_R.append(har_mean)
        har_mean = harmonic_mean(g_mean)
        harmonic_mean_G.append(har_mean)
        har_mean = harmonic_mean(b_mean)
        harmonic_mean_B.append(har_mean)
        
        Mx = Max(r_mean)
        Max_R.append(Mx)
        Mx = Max(g_mean)
        Max_G.append(Mx)
        Mx = Max(b_mean)
        Max_B.append(Mx)
        
        Mn = Min(r_mean)
        Min_R.append(Mn)
        Mn = Min(g_mean)
        Min_G.append(Mn)
        Mn = Min(b_mean)
        Min_B.append(Mn)
        
        shan = shannon_entropy(r_mean)
        shannon_entropy_R.append(shan)
        shan = shannon_entropy(g_mean)
        shannon_entropy_G.append(shan)
        shan = shannon_entropy(b_mean)
        shannon_entropy_B.append(shan)
        
        hart = hartley_func(r_mean)
        hartley_func_R.append(hart)
        hart = hartley_func(g_mean)
        hartley_func_G.append(hart)
        hart = hartley_func(b_mean)
        hartley_func_B.append(hart)
        
        per = percentile_50th(r_mean)
        percentile_50th_R.append(per)
        per = percentile_50th(g_mean)
        percentile_50th_G.append(per)
        per = percentile_50th(b_mean)
        percentile_50th_B.append(per)
        
        


'''Create the DataFrame for R channel'''
dataFrame_R_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_R": wave_energy_1st_R, 
    "wave_energy_2nd_R": wave_energy_2nd_R,
    "wave_energy_3rd_R": wave_energy_3rd_R,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_R": one_sided_spectrum_magnitude_1st_R,
    "one_sided_spectrum_magnitude_2nd_R": one_sided_spectrum_magnitude_2nd_R,
    "one_sided_spectrum_magnitude_3rd_R": one_sided_spectrum_magnitude_3rd_R,
    #========================= Statistics Analysis =============================
    "Mean_R": Mean_R,
    
    "Variance_R": Variance_R,
    
    "pVariance_R": pVariance_R,

    "Skewness_R": Skewness_R,

    "Kurtosis_R": Kurtosis_R,

    "stdev_R": stdev_R,
    
    "pstdev_R": pstdev_R,
    
    "median_R": median_R,
    
    "median_low_R": median_low_R,
    
    "median_high_R": median_high_R,
    
    "median_grouped_R": median_grouped_R,
    
    "harmonic_mean_R": harmonic_mean_R,
    
    "Max_R": Max_R,
    
    "Min_R": Min_R,
    
    "shannon_entropy_R": shannon_entropy_R, 
    
    "hartley_func_R": hartley_func_R,
    
    "percentile_50th_R": percentile_50th_R

})
    
dataFrame_R_channel.to_csv('CSV_Files/Statistics_Data/R_Statistics_LED-1070.csv')

'''Create the DataFrame for G channel'''
dataFrame_G_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_G": wave_energy_1st_G, 
    "wave_energy_2nd_G": wave_energy_2nd_G,
    "wave_energy_3rd_G": wave_energy_3rd_G,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_G": one_sided_spectrum_magnitude_1st_G,
    "one_sided_spectrum_magnitude_2nd_G": one_sided_spectrum_magnitude_2nd_G,
    "one_sided_spectrum_magnitude_3rd_G": one_sided_spectrum_magnitude_3rd_G,
    #========================= Statistics Analysis =============================
    "Mean_G": Mean_G,
    
    "Variance_G": Variance_G,
    
    "pVariance_G": pVariance_G,

    "Skewness_G": Skewness_G,

    "Kurtosis_G": Kurtosis_G,

    "stdev_G": stdev_G,
    
    "pstdev_G": pstdev_G,
    
    "median_G": median_G,
    
    "median_low_G": median_low_G,
    
    "median_high_G": median_high_G,
    
    "median_grouped_G": median_grouped_G,
    
    "harmonic_mean_G": harmonic_mean_G,
    
    "Max_G": Max_G,
    
    "Min_G": Min_G,
    
    "shannon_entropy_G": shannon_entropy_G, 
    
    "hartley_func_G": hartley_func_G,
    
    "percentile_50th_G": percentile_50th_G

})
    
dataFrame_G_channel.to_csv('CSV_Files/Statistics_Data/G_Statistics_LED-1070.csv')

'''Create the DataFrame for B channel'''
dataFrame_B_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_B": wave_energy_1st_B, 
    "wave_energy_2nd_B": wave_energy_2nd_B,
    "wave_energy_3rd_B": wave_energy_3rd_B,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_B": one_sided_spectrum_magnitude_1st_B,
    "one_sided_spectrum_magnitude_2nd_B": one_sided_spectrum_magnitude_2nd_B,
    "one_sided_spectrum_magnitude_3rd_B": one_sided_spectrum_magnitude_3rd_B,
    #========================= Statistics Analysis =============================
    "Mean_B": Mean_B,
    
    "Variance_B": Variance_B,
    
    "pVariance_B": pVariance_B,

    "Skewness_B": Skewness_B,

    "Kurtosis_B": Kurtosis_B,

    "stdev_B": stdev_B,
    
    "pstdev_B": pstdev_B,
    
    "median_B": median_B,
    
    "median_low_B": median_low_B,
    
    "median_high_B": median_high_B,
    
    "median_grouped_B": median_grouped_B,
    
    "harmonic_mean_B": harmonic_mean_B,
    
    "Max_B": Max_B,
    
    "Min_B": Min_B,
    
    "shannon_entropy_B": shannon_entropy_B, 
    
    "hartley_func_B": hartley_func_B,
    
    "percentile_50th_B": percentile_50th_B

})
    
dataFrame_B_channel.to_csv('CSV_Files/Statistics_Data/B_Statistics_LED-1070.csv')


##/////////////////////////////////////////////
##                   LED-NO
##////////////////////////////////////////////

LED_Broad_num = ["/LED-NO"]
#LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"] #, "/LED-0940", "/LED-1070","/LED-NO"

Sub_ID = []

#===========================Dominant 3 energy=====================================
wave_energy_1st_R = []
wave_energy_1st_G = []
wave_energy_1st_B = []

wave_energy_2nd_R = []
wave_energy_2nd_G = []
wave_energy_2nd_B = []

wave_energy_3rd_R = []
wave_energy_3rd_G = []
wave_energy_3rd_B = []

#===========================Dominant 3 one_sided_spectrum_magnitude ==========
one_sided_spectrum_magnitude_1st_R = []
one_sided_spectrum_magnitude_1st_G = []
one_sided_spectrum_magnitude_1st_B = []

one_sided_spectrum_magnitude_2nd_R = []
one_sided_spectrum_magnitude_2nd_G = []
one_sided_spectrum_magnitude_2nd_B = []

one_sided_spectrum_magnitude_3rd_R = []
one_sided_spectrum_magnitude_3rd_G = []
one_sided_spectrum_magnitude_3rd_B = []

#========================= Statistics Analysis =============================
Mean_R = []
Mean_G = []
Mean_B = []

Variance_R = []
Variance_G = []
Variance_B = []

pVariance_R = []
pVariance_G = []
pVariance_B = []

Skewness_R = []
Skewness_G = []
Skewness_B = []

Kurtosis_R = []
Kurtosis_G = []
Kurtosis_B = []

stdev_R = []
stdev_G = []
stdev_B = []

pstdev_R = []
pstdev_G = []
pstdev_B = []


median_R = []
median_G = []
median_B = []

median_low_R = []
median_low_G = []
median_low_B = []

median_high_R = []
median_high_G = []
median_high_B = []

median_grouped_R = []
median_grouped_G = []
median_grouped_B = []

harmonic_mean_R = []
harmonic_mean_G = []
harmonic_mean_B = []

Max_R = []
Max_G = []
Max_B = []

Min_R = []
Min_G = []
Min_B = []

shannon_entropy_R = []
shannon_entropy_G = []
shannon_entropy_B = []

hartley_func_R = []
hartley_func_G = []
hartley_func_B = []

percentile_50th_R = []
percentile_50th_G = []
percentile_50th_B = []


for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    
    for folder in folList:
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
            

        
        
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
    
        Sub_ID.append(int(ID))
        
        #############################
        #    R channel             #            
        #############################
        print("==============For R channel:===================")
#        plot_time_series(r_mean, "r", "R Channnel")
        
        ################## Dominant 3 frequency(Energy and magnitude) #############################
        '''R channel'''
        top_3_waves = Dominant_3_Waves(r_mean) # R channel
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_R.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_R.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_R.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_R.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_R.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_R.append(one_sided_spectrum_magnitude_3)
        
        '''G channel'''
        top_3_waves = Dominant_3_Waves(g_mean) # G channel 
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_G.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_G.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_G.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_G.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_G.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_G.append(one_sided_spectrum_magnitude_3)
        
        '''B channel'''
        top_3_waves = Dominant_3_Waves(b_mean) # B channel 
#        print(top_3_waves)
        
        wave_energy_1 = top_3_waves[0]["wave_energy"]
        wave_energy_1st_B.append(wave_energy_1)
        
        wave_energy_2 = top_3_waves[1]["wave_energy"]
        wave_energy_2nd_B.append(wave_energy_2)
        
        wave_energy_3 = top_3_waves[2]["wave_energy"]
        wave_energy_3rd_B.append(wave_energy_3)
        
        one_sided_spectrum_magnitude_1 = top_3_waves[0]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_1st_B.append(one_sided_spectrum_magnitude_1)
        
        one_sided_spectrum_magnitude_2 = top_3_waves[1]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_2nd_B.append(one_sided_spectrum_magnitude_2)
        
        one_sided_spectrum_magnitude_3 = top_3_waves[2]['one_sided_spectrum_magnitude']
        one_sided_spectrum_magnitude_3rd_B.append(one_sided_spectrum_magnitude_3)
        
        #========================= Statistics Analysis =============================
        Mean = mean(r_mean)
        Mean_R.append(Mean)
        Mean = mean(g_mean)
        Mean_G.append(Mean)
        Mean = mean(b_mean)
        Mean_B.append(Mean)
        
        Var = variance(r_mean)
        Variance_R.append(Var)
        Var = variance(g_mean)
        Variance_G.append(Var)
        Var = variance(b_mean)
        Variance_B.append(Var)
        
        pVar = pvariance(r_mean)
        pVariance_R.append(pVar)
        pVar = pvariance(g_mean)
        pVariance_G.append(pVar)
        pVar = pvariance(b_mean)
        pVariance_B.append(pVar)
        
        Skew = skewness(r_mean)
        Skewness_R.append(Skew)
        Skew = skewness(g_mean)
        Skewness_G.append(Skew)
        Skew = skewness(b_mean)
        Skewness_B.append(Skew)
        
        Kur = kurtosis(r_mean)
        Kurtosis_R.append(Kur)
        Kur = kurtosis(g_mean)
        Kurtosis_G.append(Kur)
        Kur = kurtosis(b_mean)
        Kurtosis_B.append(Kur)
        
        std = stdev(r_mean)
        stdev_R.append(std)
        std = stdev(g_mean)
        stdev_G.append(std)
        std = stdev(b_mean)
        stdev_B.append(std)
        
        pstd = pstdev(r_mean)
        pstdev_R.append(pstd)
        pstd = pstdev(g_mean)
        pstdev_G.append(pstd)
        pstd = pstdev(b_mean)
        pstdev_B.append(pstd)
        
        Median = median(r_mean)
        median_R.append(Median)
        Median = median(g_mean)
        median_G.append(Median)
        Median = median(b_mean)
        median_B.append(Median)
        
        Med_low = median_low(r_mean)
        median_low_R.append(Med_low)
        Med_low = median_low(g_mean)
        median_low_G.append(Med_low)
        Med_low = median_low(b_mean)
        median_low_B.append(Med_low)
        
        Med_high = median_high(r_mean)
        median_high_R.append(Med_high)
        Med_high = median_high(g_mean)
        median_high_G.append(Med_high)
        Med_high = median_high(b_mean)
        median_high_B.append(Med_high)
        
        Med_grp = median_grouped(r_mean)
        median_grouped_R.append(Med_grp)
        Med_grp = median_grouped(g_mean)
        median_grouped_G.append(Med_grp)
        Med_grp = median_grouped(b_mean)
        median_grouped_B.append(Med_grp)
        
        har_mean = harmonic_mean(r_mean)
        harmonic_mean_R.append(har_mean)
        har_mean = harmonic_mean(g_mean)
        harmonic_mean_G.append(har_mean)
        har_mean = harmonic_mean(b_mean)
        harmonic_mean_B.append(har_mean)
        
        Mx = Max(r_mean)
        Max_R.append(Mx)
        Mx = Max(g_mean)
        Max_G.append(Mx)
        Mx = Max(b_mean)
        Max_B.append(Mx)
        
        Mn = Min(r_mean)
        Min_R.append(Mn)
        Mn = Min(g_mean)
        Min_G.append(Mn)
        Mn = Min(b_mean)
        Min_B.append(Mn)
        
        shan = shannon_entropy(r_mean)
        shannon_entropy_R.append(shan)
        shan = shannon_entropy(g_mean)
        shannon_entropy_G.append(shan)
        shan = shannon_entropy(b_mean)
        shannon_entropy_B.append(shan)
        
        hart = hartley_func(r_mean)
        hartley_func_R.append(hart)
        hart = hartley_func(g_mean)
        hartley_func_G.append(hart)
        hart = hartley_func(b_mean)
        hartley_func_B.append(hart)
        
        per = percentile_50th(r_mean)
        percentile_50th_R.append(per)
        per = percentile_50th(g_mean)
        percentile_50th_G.append(per)
        per = percentile_50th(b_mean)
        percentile_50th_B.append(per)
        
        


'''Create the DataFrame for R channel'''
dataFrame_R_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_R": wave_energy_1st_R, 
    "wave_energy_2nd_R": wave_energy_2nd_R,
    "wave_energy_3rd_R": wave_energy_3rd_R,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_R": one_sided_spectrum_magnitude_1st_R,
    "one_sided_spectrum_magnitude_2nd_R": one_sided_spectrum_magnitude_2nd_R,
    "one_sided_spectrum_magnitude_3rd_R": one_sided_spectrum_magnitude_3rd_R,
    #========================= Statistics Analysis =============================
    "Mean_R": Mean_R,
    
    "Variance_R": Variance_R,
    
    "pVariance_R": pVariance_R,

    "Skewness_R": Skewness_R,

    "Kurtosis_R": Kurtosis_R,

    "stdev_R": stdev_R,
    
    "pstdev_R": pstdev_R,
    
    "median_R": median_R,
    
    "median_low_R": median_low_R,
    
    "median_high_R": median_high_R,
    
    "median_grouped_R": median_grouped_R,
    
    "harmonic_mean_R": harmonic_mean_R,
    
    "Max_R": Max_R,
    
    "Min_R": Min_R,
    
    "shannon_entropy_R": shannon_entropy_R, 
    
    "hartley_func_R": hartley_func_R,
    
    "percentile_50th_R": percentile_50th_R

})
    
dataFrame_R_channel.to_csv('CSV_Files/Statistics_Data/R_Statistics_LED-NO.csv')

'''Create the DataFrame for G channel'''
dataFrame_G_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_G": wave_energy_1st_G, 
    "wave_energy_2nd_G": wave_energy_2nd_G,
    "wave_energy_3rd_G": wave_energy_3rd_G,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_G": one_sided_spectrum_magnitude_1st_G,
    "one_sided_spectrum_magnitude_2nd_G": one_sided_spectrum_magnitude_2nd_G,
    "one_sided_spectrum_magnitude_3rd_G": one_sided_spectrum_magnitude_3rd_G,
    #========================= Statistics Analysis =============================
    "Mean_G": Mean_G,
    
    "Variance_G": Variance_G,
    
    "pVariance_G": pVariance_G,

    "Skewness_G": Skewness_G,

    "Kurtosis_G": Kurtosis_G,

    "stdev_G": stdev_G,
    
    "pstdev_G": pstdev_G,
    
    "median_G": median_G,
    
    "median_low_G": median_low_G,
    
    "median_high_G": median_high_G,
    
    "median_grouped_G": median_grouped_G,
    
    "harmonic_mean_G": harmonic_mean_G,
    
    "Max_G": Max_G,
    
    "Min_G": Min_G,
    
    "shannon_entropy_G": shannon_entropy_G, 
    
    "hartley_func_G": hartley_func_G,
    
    "percentile_50th_G": percentile_50th_G

})
    
dataFrame_G_channel.to_csv('CSV_Files/Statistics_Data/G_Statistics_LED-NO.csv')

'''Create the DataFrame for B channel'''
dataFrame_B_channel = pd.DataFrame({
    'ID': Sub_ID,
    #===========================Dominant 3 energy=====================================
    "wave_energy_1st_B": wave_energy_1st_B, 
    "wave_energy_2nd_B": wave_energy_2nd_B,
    "wave_energy_3rd_B": wave_energy_3rd_B,
    #===========================Dominant 3 one_sided_spectrum_magnitude ========
    "one_sided_spectrum_magnitude_1st_B": one_sided_spectrum_magnitude_1st_B,
    "one_sided_spectrum_magnitude_2nd_B": one_sided_spectrum_magnitude_2nd_B,
    "one_sided_spectrum_magnitude_3rd_B": one_sided_spectrum_magnitude_3rd_B,
    #========================= Statistics Analysis =============================
    "Mean_B": Mean_B,
    
    "Variance_B": Variance_B,
    
    "pVariance_B": pVariance_B,

    "Skewness_B": Skewness_B,

    "Kurtosis_B": Kurtosis_B,

    "stdev_B": stdev_B,
    
    "pstdev_B": pstdev_B,
    
    "median_B": median_B,
    
    "median_low_B": median_low_B,
    
    "median_high_B": median_high_B,
    
    "median_grouped_B": median_grouped_B,
    
    "harmonic_mean_B": harmonic_mean_B,
    
    "Max_B": Max_B,
    
    "Min_B": Min_B,
    
    "shannon_entropy_B": shannon_entropy_B, 
    
    "hartley_func_B": hartley_func_B,
    
    "percentile_50th_B": percentile_50th_B

})
    
dataFrame_B_channel.to_csv('CSV_Files/Statistics_Data/B_Statistics_LED-NO.csv')