
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
df_Clinical = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Shared AmaderGram Data 2018-DataSheet - Clinical data.csv")


# ## Clinical Report

# In[2]:


df_Clinical.describe()


# In[3]:


df_Clinical.head()


# In[4]:


df_Clinical.columns


# In[5]:


df_Clinical.shape


# ## PPG feature Extraction

# In[7]:


df_PPG_R_0850 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/table_PPG_Smooth_R_0850.csv")
df_PPG_G_0850 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/table_PPG_Smooth_G_0850.csv")
df_PPG_B_0850 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0850_PPG_CSV/table_PPG_Smooth_B_0850.csv")

df_PPG_R_0940 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/table_PPG_Smooth_R_0940.csv")
df_PPG_G_0940 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/table_PPG_Smooth_G_0940.csv")
df_PPG_B_0940 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-0940_PPG_CSV/table_PPG_Smooth_B_0940.csv")

df_PPG_R_1070 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/table_PPG_Smooth_R_1070.csv")
df_PPG_G_1070 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/table_PPG_Smooth_G_1070.csv")
df_PPG_B_1070 = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-1070_PPG_CSV/table_PPG_Smooth_B_1070.csv")

df_PPG_R_NO = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/table_PPG_Smooth_R_NO.csv")
df_PPG_G_NO = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/table_PPG_Smooth_G_NO.csv")
df_PPG_B_NO = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/LED-NO_PPG_CSV/table_PPG_Smooth_B_NO.csv")


# In[8]:


df_PPG_R_0850_merge = pd.merge(df_PPG_R_0850, df_Clinical, on='Sub_ID', how='outer')
df_PPG_G_0850_merge = pd.merge(df_PPG_G_0850, df_Clinical, on='Sub_ID', how='outer')
df_PPG_B_0850_merge = pd.merge(df_PPG_B_0850, df_Clinical, on='Sub_ID', how='outer')

df_PPG_R_0940_merge = pd.merge(df_PPG_R_0940, df_Clinical, on='Sub_ID', how='outer')
df_PPG_G_0940_merge = pd.merge(df_PPG_G_0940, df_Clinical, on='Sub_ID', how='outer')
df_PPG_B_0940_merge = pd.merge(df_PPG_B_0940, df_Clinical, on='Sub_ID', how='outer')

df_PPG_R_1070_merge = pd.merge(df_PPG_R_1070, df_Clinical, on='Sub_ID', how='outer')
df_PPG_G_1070_merge = pd.merge(df_PPG_G_1070, df_Clinical, on='Sub_ID', how='outer')
df_PPG_B_1070_merge = pd.merge(df_PPG_B_1070, df_Clinical, on='Sub_ID', how='outer')

df_PPG_R_NO_merge = pd.merge(df_PPG_R_NO, df_Clinical, on='Sub_ID', how='outer')
df_PPG_G_NO_merge = pd.merge(df_PPG_G_NO, df_Clinical, on='Sub_ID', how='outer')
df_PPG_B_NO_merge = pd.merge(df_PPG_B_NO, df_Clinical, on='Sub_ID', how='outer')


# In[9]:


col_list = ['Sub_ID', 'Age', 'Sex', 'Systolic_peak(x)', 'Diastolic_peak(y)',
       'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)',
       'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x',
       'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)',
       'Dicrotic_notch_time(t3)',
       'Time_between_systolic_and_diastolic_peaks(∆T)',
       'Time_between_half_systolic_peak_points(w)',
       'Inflection_point_area_ratio(A2/A1)',
       'Systolic_peak_rising_slope(t1/x)',
       'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi',
       't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2',
       '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi',
       'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi',
       '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)',
       'Fundamental_component_magnitude(|sbase|)',
       '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)',
       '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)',
       'Stress-induced_vascular_response_index(sVRI)',  'Hb (gm/dL)']
print(len(col_list))


# In[10]:


dataFrame_PPG_R_0850 = df_PPG_R_0850_merge[col_list]
dataFrame_PPG_G_0850 = df_PPG_G_0850_merge[col_list]
dataFrame_PPG_B_0850 = df_PPG_B_0850_merge[col_list]

dataFrame_PPG_R_0940 = df_PPG_R_0940_merge[col_list]
dataFrame_PPG_G_0940 = df_PPG_G_0940_merge[col_list]
dataFrame_PPG_B_0940 = df_PPG_B_0940_merge[col_list]

dataFrame_PPG_R_1070 = df_PPG_R_1070_merge[col_list]
dataFrame_PPG_G_1070 = df_PPG_G_1070_merge[col_list]
dataFrame_PPG_B_1070 = df_PPG_B_1070_merge[col_list]

dataFrame_PPG_R_NO = df_PPG_R_NO_merge[col_list]
dataFrame_PPG_G_NO = df_PPG_G_NO_merge[col_list]
dataFrame_PPG_B_NO = df_PPG_B_NO_merge[col_list]


# In[16]:


dataFrame_PPG_R_0850.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_R_0850.csv")
dataFrame_PPG_G_0850.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_G_0850.csv")
dataFrame_PPG_B_0850.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_B_0850.csv")

dataFrame_PPG_R_0940.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_R_0940.csv")
dataFrame_PPG_G_0940.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_G_0940.csv")
dataFrame_PPG_B_0940.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_B_0940.csv")

dataFrame_PPG_R_1070.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_R_1070.csv")
dataFrame_PPG_G_1070.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_G_1070.csv")
dataFrame_PPG_B_1070.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_B_1070.csv")

dataFrame_PPG_R_NO.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_R_NO.csv")
dataFrame_PPG_G_NO.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_G_NO.csv")
dataFrame_PPG_B_NO.iloc[0:135, ].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/dataFrame_PPG_B_NO.csv")

