
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
df_Clinical = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Shared AmaderGram Data 2018-DataSheet - Clinical data.csv")


# ## Clinical Report

# In[2]:


df_Clinical.describe()


# In[3]:


df_Clinical.head()


# In[4]:


df_Clinical.columns


# ## High pass Filter (Max/Min)

# In[5]:


df_HighPass_Max_Min_R = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/R_channel_HighPass_Max_Min.csv")
df_HighPass_Max_Min_G = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/G_channel_HighPass_Max_Min.csv")
df_HighPass_Max_Min_B = pd.read_csv("/media/rezwan/Softwares/THESIS/CSV_Files/B_channel_HighPass_Max_Min.csv")


# In[6]:


df_HighPass_Max_Min_R_merge = pd.merge(df_HighPass_Max_Min_R, df_Clinical, on='Sub_ID', how='outer')
df_HighPass_Max_Min_G_merge = pd.merge(df_HighPass_Max_Min_G, df_Clinical, on='Sub_ID', how='outer')
df_HighPass_Max_Min_B_merge = pd.merge(df_HighPass_Max_Min_B, df_Clinical, on='Sub_ID', how='outer')


# In[7]:


df_HighPass_Max_Min_R_merge.columns


# In[8]:


col_list = ['Sub_ID', 'Age', 'Sex','(Max/Min)_0850', '(Max/Min)_0940', '(Max/Min)_1070','(Max/Min)_NO', 'Hb (gm/dL)']
print(len(col_list))


# In[9]:


df_HighPass_Max_Min_R = df_HighPass_Max_Min_R_merge[col_list]
df_HighPass_Max_Min_G = df_HighPass_Max_Min_G_merge[col_list]
df_HighPass_Max_Min_B = df_HighPass_Max_Min_B_merge[col_list]


# In[17]:


df_HighPass_Max_Min_R.iloc[0:137,].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/HighPass/df_HighPass_Max_Min_R.csv")
df_HighPass_Max_Min_G.iloc[0:137,].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/HighPass/df_HighPass_Max_Min_G.csv")
df_HighPass_Max_Min_B.iloc[0:137,].to_csv("/media/rezwan/Softwares/THESIS/CSV_Files/Preprocess_csv/HighPass/df_HighPass_Max_Min_B.csv")

