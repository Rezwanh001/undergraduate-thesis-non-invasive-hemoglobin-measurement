from libraries import *
from video_to_frames import *
from plot_peak_frq import *
from plot_time_series import *
from high_pass_filter import *
from FFT import *
from peakdetect import *
from Moving_Average import *

#### Enter the Sourse and Destination folder
src_dir = "LED-Broad"
dst_dir = "Extract-Images"
dataFrameArr = []

# Enter the LED-Broad
LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"]

################################
#       Extract Frames         #
################################

for broad in LED_Broad_num:
    sourceDir = src_dir + broad + "/*.mp4"
    destinationDir = dst_dir + broad
    print(destinationDir)
    
    if not os.path.exists(destinationDir):
        os.mkdir(destinationDir)
        
    vList = glob.glob(sourceDir)
    # print(vList)
    
    for i in range(len(vList)):
        vDirName = vList[i]
        head, tail = os.path.split(vDirName)
        # get frames from video
        path = video_to_frames(vDirName, destinationDir, tail)
        dataFrameArr.append(path)

print(dataFrameArr) ### Load the directory of all image's frame file.
################################
#     End Extract Frames       #
################################

## Load Sorted file name into the Array
#Files = []


########################## Declare List for creating Dataframe ########

##################################################################################
'''                R Channel    '''
##################################################################################

'''R channel'''
LED_0850_R = []
LED_0940_R = []
LED_1070_R = []
LED_NO_R = []

ID_LST_R = []

""" CalCulate for Only R channel. """

for path in dataFrameArr:
    print(path)
    file_names = []
    for file in os.listdir(path):
        file_names.append(file)
        
    file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
    #Files.append(file_names)
    #print(len(file_names))
    
    r_mean = []
    g_mean = []
    b_mean = []
    
    seq_num = []
    
    file_cnt = 0
    for file in file_names:
        img = cv2.imread(os.path.join(path,file))
        
        #print(file[:-4])
        
        if file_cnt == 600: # Take 600 frames.
            break
            
        file_cnt += 1
        
        seq_num.append(int(file[:-4]))
        
        average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
        
        #BGR
        b_mean.append(average_color[0])
        g_mean.append(average_color[1])
        r_mean.append(average_color[2])
        
    ''' # Plot the time series of each channel    
    ### Plot the Red channel time series
    plot_time_series(r_mean, "r", "Red Channel")
    
    ### Plot the Green channel time series
    plot_time_series(g_mean, "g", "Green Channel")
    
    ### Plot the Blue channel time series
    plot_time_series(b_mean, "b", "Blue Channel")
    '''
    
    ##### High Pass filter apply on each channel ####################
    fps = 60
    cutoff = 0.5
    high_pass_filter_R = butter_highpass_filter(r_mean, cutoff, fps)
    # plot_time_series(high_pass_filter_R, 'r', label='High pass filter for R channel')
    
    high_pass_filter_G = butter_highpass_filter(g_mean, cutoff, fps)
    # plot_time_series(high_pass_filter_G, 'g', label='High pass filter for G channel')
    
    high_pass_filter_B = butter_highpass_filter(b_mean, cutoff, fps)
    # plot_time_series(high_pass_filter_B, 'b', label='High pass filter for B channel')
    
    
    ############################ Moving Average Filter ##############
#    moving_avg_R = movingaverage(r_mean, 3)
#    plot_time_series(moving_avg_R, 'r', label='Moving Average Filter for R channel')
#    
#    
#    emafast, emaslow, diff = computeMACD(r_mean)
#    print(emafast)
    
    
    ############## Fast FFT on high pass filtered channel ###############
    FFT_R = FFT(high_pass_filter_R)
    #plot_time_series(FFT_R, 'r', label='FFT for R channel')
    
    FFT_G = FFT(high_pass_filter_G)
    #plot_time_series(FFT_G, 'g', label='FFT for G channel')
    
    FFT_B = FFT(high_pass_filter_B)
    #plot_time_series(FFT_B, 'b', label='FFT for B channel')
    
    
    ##############################
    LED_Broad = path.split('/')[1].split('-')[1]
    print("LED Broad: " + str(LED_Broad))
    
    ID = path.split('/')[2].split('-')[0]
    print("ID: " + str(ID))
    
#    if LED_Broad == "0850":
    
    ####### Find Dominant Peak From Filtered channels ##############
    '''
    R Channel
    '''
    y = high_pass_filter_R
    #y = FFT_R
    x = range(len(high_pass_filter_R))
    _max, _min = peakdetect(y,x,1, 0.0)
    #print(_max, _min)
    max_peak = abs(max(_max,key=lambda item:item[1])[1])
    min_peak = abs(min(_min,key=lambda item:item[1])[1])
    log_of_max_min = np.log(max_peak / min_peak)
#    print("Max dominant for R : " + str(max(_max,key=lambda item:item[1])[1]))
#    print("Min dominant for R : " + str(min(_min,key=lambda item:item[1])[1]))
    print("Log of Max/Min peak for R : " + str(log_of_max_min))
#    xm = [p[0] for p in _max]
#    ym = [p[1] for p in _max]
#    xn = [p[0] for p in _min]
#    yn = [p[1] for p in _min]
#    plot_peak_frq(y,x, xm, xn, ym, yn, "r", "Peak for R channel")
    
    if LED_Broad == '0850':
        LED_0850_R.append(log_of_max_min)
    elif LED_Broad == '0940':
        LED_0940_R.append(log_of_max_min)
    elif LED_Broad == '1070':
        LED_1070_R.append(log_of_max_min)
    elif LED_Broad == 'NO':
        LED_NO_R.append(log_of_max_min)
        
    if ID not in ID_LST_R:
        ID_LST_R.append(ID)
    
    
print(ID_LST_R)
print(LED_0850_R)
print(LED_0940_R)
print(LED_1070_R)
print(LED_NO_R)

'''Create the DataFrame for R channel'''
dataFrame_R_channel = pd.DataFrame({
            'ID': ID_LST_R,
            'Log(Max/Min)_0850': LED_0850_R,
            'Log(Max/Min)_0940': LED_0940_R,
            'Log(Max/Min)_1070': LED_1070_R,
            'Log(Max/Min)_NO': LED_NO_R
        })
dataFrame_R_channel.to_csv('CSV_Files/R_channel.csv')



##################################################################################
'''                G Channel    '''
##################################################################################

'''G channel'''
LED_0850_G = []
LED_0940_G = []
LED_1070_G = []
LED_NO_G = []

ID_LST_G = []

""" CalCulate for Only G channel. """

for path in dataFrameArr:
    print(path)
    file_names = []
    for file in os.listdir(path):
        file_names.append(file)
        
    file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
    #Files.append(file_names)
    #print(len(file_names))
    
    r_mean = []
    g_mean = []
    b_mean = []
    
    seq_num = []
    
    file_cnt = 0
    for file in file_names:
        img = cv2.imread(os.path.join(path,file))
        
        #print(file[:-4])
        
        if file_cnt == 600: # Take 600 frames.
            break
            
        file_cnt += 1
        
        seq_num.append(int(file[:-4]))
        
        average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
        
        #BGR
        b_mean.append(average_color[0])
        g_mean.append(average_color[1])
        r_mean.append(average_color[2])
        
    ''' # Plot the time series of each channel    
    ### Plot the Red channel time series
    plot_time_series(r_mean, "r", "Red Channel")
    
    ### Plot the Green channel time series
    plot_time_series(g_mean, "g", "Green Channel")
    
    ### Plot the Blue channel time series
    plot_time_series(b_mean, "b", "Blue Channel")
    '''
    
    ##### High Pass filter apply on each channel ####################
    fps = 60
    cutoff = 0.5
    high_pass_filter_R = butter_highpass_filter(r_mean, cutoff, fps)
    # plot_time_series(high_pass_filter_R, 'r', label='High pass filter for R channel')
    
    high_pass_filter_G = butter_highpass_filter(g_mean, cutoff, fps)
    # plot_time_series(high_pass_filter_G, 'g', label='High pass filter for G channel')
    
    high_pass_filter_B = butter_highpass_filter(b_mean, cutoff, fps)
    # plot_time_series(high_pass_filter_B, 'b', label='High pass filter for B channel')
    
    
    ############## Fast FFT on high pass filtered channel ###############
    FFT_R = FFT(high_pass_filter_R)
    #plot_time_series(FFT_R, 'r', label='FFT for R channel')
    
    FFT_G = FFT(high_pass_filter_G)
    #plot_time_series(FFT_G, 'g', label='FFT for G channel')
    
    FFT_B = FFT(high_pass_filter_B)
    #plot_time_series(FFT_B, 'b', label='FFT for B channel')
    
    
    ##############################
    LED_Broad = path.split('/')[1].split('-')[1]
    print("LED Broad: " + str(LED_Broad))
    
    ID = path.split('/')[2].split('-')[0]
    print("ID: " + str(ID))
    
#    if LED_Broad == "0850":
    
    ####### Find Dominant Peak From Filtered channels ##############
    '''
    G Channel
    '''
    y = high_pass_filter_G
    #y = FFT_R
    x = range(len(high_pass_filter_G))
    _max, _min = peakdetect(y,x,1, 0.0)
    #print(_max, _min)
    max_peak = abs(max(_max,key=lambda item:item[1])[1])
    min_peak = abs(min(_min,key=lambda item:item[1])[1])
    log_of_max_min = np.log(max_peak / min_peak)
#    print("Max dominant for G : " + str(max(_max,key=lambda item:item[1])[1]))
#    print("Min dominant for G : " + str(min(_min,key=lambda item:item[1])[1]))
    print("Log of Max/Min peak for G : " + str(log_of_max_min))
#    xm = [p[0] for p in _max]
#    ym = [p[1] for p in _max]
#    xn = [p[0] for p in _min]
#    yn = [p[1] for p in _min]
#    plot_peak_frq(y,x, xm, xn, ym, yn, "r", "Peak for R channel")

    if LED_Broad == '0850':
        LED_0850_G.append(log_of_max_min)
    elif LED_Broad == '0940':
        LED_0940_G.append(log_of_max_min)
    elif LED_Broad == '1070':
        LED_1070_G.append(log_of_max_min)
    elif LED_Broad == 'NO':
        LED_NO_G.append(log_of_max_min)
        
    if ID not in ID_LST_G:
        ID_LST_G.append(ID)

 
print(ID_LST_G)
print(LED_0850_G)
print(LED_0940_G)
print(LED_1070_G)
print(LED_NO_G)

'''Create the DataFrame for G channel'''
dataFrame_G_channel = pd.DataFrame({
            'ID': ID_LST_G,
            'Log(Max/Min)_0850': LED_0850_G,
            'Log(Max/Min)_0940': LED_0940_G,
            'Log(Max/Min)_1070': LED_1070_G,
            'Log(Max/Min)_NO': LED_NO_G
        })
dataFrame_G_channel.to_csv('CSV_Files/G_channel.csv')



##################################################################################
'''                B Channel    '''
##################################################################################

'''B channel'''
LED_0850_B = []
LED_0940_B = []
LED_1070_B = []
LED_NO_B = []

ID_LST_B = []

""" CalCulate for Only B channel. """

for path in dataFrameArr:
    print(path)
    file_names = []
    for file in os.listdir(path):
        file_names.append(file)
        
    file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
    #Files.append(file_names)
    #print(len(file_names))
    
    r_mean = []
    g_mean = []
    b_mean = []
    
    seq_num = []
    
    file_cnt = 0
    for file in file_names:
        img = cv2.imread(os.path.join(path,file))
        
        #print(file[:-4])
        
        if file_cnt == 600: # Take 600 frames.
            break
            
        file_cnt += 1
        
        seq_num.append(int(file[:-4]))
        
        average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
        
        #BGR
        b_mean.append(average_color[0])
        g_mean.append(average_color[1])
        r_mean.append(average_color[2])
        
    ''' # Plot the time series of each channel    
    ### Plot the Red channel time series
    plot_time_series(r_mean, "r", "Red Channel")
    
    ### Plot the Green channel time series
    plot_time_series(g_mean, "g", "Green Channel")
    
    ### Plot the Blue channel time series
    plot_time_series(b_mean, "b", "Blue Channel")
    '''
    
    ##### High Pass filter apply on each channel ####################
    fps = 60
    cutoff = 0.5
    high_pass_filter_R = butter_highpass_filter(r_mean, cutoff, fps)
    # plot_time_series(high_pass_filter_R, 'r', label='High pass filter for R channel')
    
    high_pass_filter_G = butter_highpass_filter(g_mean, cutoff, fps)
    # plot_time_series(high_pass_filter_G, 'g', label='High pass filter for G channel')
    
    high_pass_filter_B = butter_highpass_filter(b_mean, cutoff, fps)
    # plot_time_series(high_pass_filter_B, 'b', label='High pass filter for B channel')
    
    
    ############## Fast FFT on high pass filtered channel ###############
    FFT_R = FFT(high_pass_filter_R)
    #plot_time_series(FFT_R, 'r', label='FFT for R channel')
    
    FFT_G = FFT(high_pass_filter_G)
    #plot_time_series(FFT_G, 'g', label='FFT for G channel')
    
    FFT_B = FFT(high_pass_filter_B)
    #plot_time_series(FFT_B, 'b', label='FFT for B channel')
    
    
    ##############################
    LED_Broad = path.split('/')[1].split('-')[1]
    print("LED Broad: " + str(LED_Broad))
    
    ID = path.split('/')[2].split('-')[0]
    print("ID: " + str(ID))
    
#    if LED_Broad == "0850":
    
    ####### Find Dominant Peak From Filtered channels ##############
    '''
    B Channel
    '''
    y = high_pass_filter_B
    #y = FFT_R
    x = range(len(high_pass_filter_B))
    _max, _min = peakdetect(y,x,1, 0.0)
    #print(_max, _min)
    max_peak = abs(max(_max,key=lambda item:item[1])[1])
    min_peak = abs(min(_min,key=lambda item:item[1])[1])
    log_of_max_min = np.log(max_peak / min_peak)
#    print("Max dominant for B : " + str(max(_max,key=lambda item:item[1])[1]))
#    print("Min dominant for B : " + str(min(_min,key=lambda item:item[1])[1]))
    print("Log of Max/Min peak for B : " + str(log_of_max_min))
#    xm = [p[0] for p in _max]
#    ym = [p[1] for p in _max]
#    xn = [p[0] for p in _min]
#    yn = [p[1] for p in _min]
#    plot_peak_frq(y,x, xm, xn, ym, yn, "r", "Peak for R channel")

    if LED_Broad == '0850':
        LED_0850_B.append(log_of_max_min)
    elif LED_Broad == '0940':
        LED_0940_B.append(log_of_max_min)
    elif LED_Broad == '1070':
        LED_1070_B.append(log_of_max_min)
    elif LED_Broad == 'NO':
        LED_NO_B.append(log_of_max_min)
        
    if ID not in ID_LST_B:
        ID_LST_B.append(ID)
    

print(ID_LST_B)
print(LED_0850_B)
print(LED_0940_B)
print(LED_1070_B)
print(LED_NO_B)

'''Create the DataFrame for B channel'''
dataFrame_B_channel = pd.DataFrame({
            'ID': ID_LST_B,
            'Log(Max/Min)_0850': LED_0850_B,
            'Log(Max/Min)_0940': LED_0940_B,
            'Log(Max/Min)_1070': LED_1070_B,
            'Log(Max/Min)_NO': LED_NO_B
        })
dataFrame_B_channel.to_csv('CSV_Files/B_channel.csv')
