#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 22:33:41 2018

@author: rezwan
"""
            
##################################################################
        
from libraries import *
from video_to_frames import *
from plot_peak_frq import *
from plot_time_series import *
from high_pass_filter import *
from FFT import *
from peakdetect import *

############################################ PPG-45
from Signal import *
from params import *
from feature import *
from learn import *

##################################################################################
'''                R Channel , G Channel, B Channel  For LED-0850    '''
##################################################################################

#########
table_PPG_NonSmooth_R = []
table_PPG_NonSmooth_G = []
table_PPG_NonSmooth_B = []

table_PPG_Smooth_R = []
table_PPG_Smooth_G = []
table_PPG_Smooth_B = []

""" Generate .csv files for  R, G, B channels respectively . """

### Select the directory
#src_dir = "Extract-Images"  
src_dir = "RawData/Extract-Images"

LED_Broad_num = ["/LED-0850"]
#LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"] #, "/LED-0940", "/LED-1070","/LED-NO"

for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    print("Path : " + str(path))
    
    for folder in folList:
        print("Folder : " + str(folder))
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
#        print(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
            
        ''' # Plot the time series of each channel    
        ### Plot the Red channel time series
        plot_time_series(r_mean, "r", "Red Channel")
        
        ### Plot the Green channel time series
        plot_time_series(g_mean, "g", "Green Channel")
        
        ### Plot the Blue channel time series
        plot_time_series(b_mean, "b", "Blue Channel")
        '''
        
        ##### High Pass filter apply on each channel ####################
        fps = 60
        cutoff = 0.5
        high_pass_filter_R = butter_highpass_filter(r_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_R, 'r', label='High pass filter for R channel')
        
        high_pass_filter_G = butter_highpass_filter(g_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_G, 'g', label='High pass filter for G channel')
        
        high_pass_filter_B = butter_highpass_filter(b_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_B, 'b', label='High pass filter for B channel')
        
        
        ############## Fast FFT on high pass filtered channel ###############
        FFT_R = FFT(high_pass_filter_R)
        #plot_time_series(FFT_R, 'r', label='FFT for R channel')
        
        FFT_G = FFT(high_pass_filter_G)
        #plot_time_series(FFT_G, 'g', label='FFT for G channel')
        
        FFT_B = FFT(high_pass_filter_B)
        #plot_time_series(FFT_B, 'b', label='FFT for B channel')
        
        
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
    
        #############################
        #    R channel             #            
        #############################
        print("==============For R channel:===================")
        plot_time_series(r_mean, "r", "R Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(r_mean) # Here the R Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform R: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "r", "extract_ppg_single_waveform_without_smooth for R")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_R.append(extract_ppg45_feat)
        
        #================================================================#
        print("==========Smooth the Signal for R channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(r_mean) # Here the R Channel For Smoothing
        plot_time_series(smooth_signal, "r", "Smoth PPG signal for R channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform R: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "r", "extract_ppg_single_waveform_with_smooth for R")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_R.append(extract_ppg45_feat)
        
        
        #############################
        #    G channel             #            
        #############################
        print("==============For G channel:===================")
        plot_time_series(g_mean, "g", "G Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(g_mean) # Here the G Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform G: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "g", "extract_ppg_single_waveform_without_smooth for G")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_G.append(extract_ppg45_feat)
        
        #================================================================#
        print("==========Smooth the Signal for G channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(g_mean) # Here the G Channel For Smoothing
        plot_time_series(smooth_signal, "g", "Smoth PPG signal for G channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform G: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "g", "extract_ppg_single_waveform_with_smooth for G")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_G.append(extract_ppg45_feat)
        print(len(table_PPG_Smooth_G))
        
        #############################
        #    B channel             #            
        #############################
        print("==============For B channel:===================")
        plot_time_series(g_mean, "b", "B Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(b_mean) # Here the B Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform B: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "b", "extract_ppg_single_waveform_without_smooth for B")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_B.append(extract_ppg45_feat)
#        print(len(table_PPG_NonSmooth_B))
        
        #================================================================#
        print("==========Smooth the Signal for B channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(b_mean) # Here the B Channel For Smoothing
        plot_time_series(smooth_signal, "b", "Smoth PPG signal for B channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform B: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "b", "extract_ppg_single_waveform_with_smooth for B")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_B.append(extract_ppg45_feat)
#        print(len(table_PPG_Smooth_B))


headers = ['ID', 'Systolic_peak(x)', 'Diastolic_peak(y)', 'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)', 'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x', 'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)', 'Dicrotic_notch_time(t3)', 'Time_between_systolic_and_diastolic_peaks(∆T)', 'Time_between_half_systolic_peak_points(w)', 'Inflection_point_area_ratio(A2/A1)', 'Systolic_peak_rising_slope(t1/x)', 'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi', 't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2', '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi', 'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi', '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)', 'Fundamental_component_magnitude(|sbase|)', '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)', '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)', 'Stress-induced_vascular_response_index(sVRI)']       

df = pd.DataFrame(table_PPG_NonSmooth_R, columns=headers)
df.to_csv('CSV_Files/LED-0850_PPG_CSV/PPG_NonSmooth_R_0850.csv')
df = pd.DataFrame(table_PPG_Smooth_R, columns=headers)
df.to_csv('CSV_Files/LED-0850_PPG_CSV/PPG_Smooth_R_0850.csv')

df = pd.DataFrame(table_PPG_NonSmooth_G, columns=headers)
df.to_csv('CSV_Files/LED-0850_PPG_CSV/PPG_NonSmooth_G_0850.csv')
df = pd.DataFrame(table_PPG_Smooth_G, columns=headers)
df.to_csv('CSV_Files/LED-0850_PPG_CSV/PPG_Smooth_G_0850.csv')

df = pd.DataFrame(table_PPG_NonSmooth_B, columns=headers)
df.to_csv('CSV_Files/LED-0850_PPG_CSV/PPG_NonSmooth_B_0850.csv')
df = pd.DataFrame(table_PPG_Smooth_B, columns=headers)
df.to_csv('CSV_Files/LED-0850_PPG_CSV/PPG_Smooth_B_0850.csv')



##################################################################################
'''                R Channel , G Channel, B Channel  For LED-0940    '''
##################################################################################

#########
table_PPG_NonSmooth_R = []
table_PPG_NonSmooth_G = []
table_PPG_NonSmooth_B = []

table_PPG_Smooth_R = []
table_PPG_Smooth_G = []
table_PPG_Smooth_B = []

""" Generate .csv files for  R, G, B channels respectively . """

#src_dir = "Extract-Images"
#src_dir = "RawData/Extract-Images"
LED_Broad_num = ["/LED-0940"]
#LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"] #, "/LED-0940", "/LED-1070","/LED-NO"

for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    print("Path : " + str(path))
    
    for folder in folList:
        print("Folder : " + str(folder))
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
#        print(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
            
        ''' # Plot the time series of each channel    
        ### Plot the Red channel time series
        plot_time_series(r_mean, "r", "Red Channel")
        
        ### Plot the Green channel time series
        plot_time_series(g_mean, "g", "Green Channel")
        
        ### Plot the Blue channel time series
        plot_time_series(b_mean, "b", "Blue Channel")
        '''
        
        ##### High Pass filter apply on each channel ####################
        fps = 60
        cutoff = 0.5
        high_pass_filter_R = butter_highpass_filter(r_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_R, 'r', label='High pass filter for R channel')
        
        high_pass_filter_G = butter_highpass_filter(g_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_G, 'g', label='High pass filter for G channel')
        
        high_pass_filter_B = butter_highpass_filter(b_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_B, 'b', label='High pass filter for B channel')
        
        
        ############## Fast FFT on high pass filtered channel ###############
        FFT_R = FFT(high_pass_filter_R)
        #plot_time_series(FFT_R, 'r', label='FFT for R channel')
        
        FFT_G = FFT(high_pass_filter_G)
        #plot_time_series(FFT_G, 'g', label='FFT for G channel')
        
        FFT_B = FFT(high_pass_filter_B)
        #plot_time_series(FFT_B, 'b', label='FFT for B channel')
        
        
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
    
        #############################
        #    R channel             #            
        #############################
        print("==============For R channel:===================")
        plot_time_series(r_mean, "r", "R Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(r_mean) # Here the R Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform R: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "r", "extract_ppg_single_waveform_without_smooth for R")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_R.append(extract_ppg45_feat)
        
        #================================================================#
        print("==========Smooth the Signal for R channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(r_mean) # Here the R Channel For Smoothing
        plot_time_series(smooth_signal, "r", "Smoth PPG signal for R channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform R: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "r", "extract_ppg_single_waveform_with_smooth for R")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_R.append(extract_ppg45_feat)
        
        
        #############################
        #    G channel             #            
        #############################
        print("==============For G channel:===================")
        plot_time_series(g_mean, "g", "G Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(g_mean) # Here the G Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform G: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "g", "extract_ppg_single_waveform_without_smooth for G")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_G.append(extract_ppg45_feat)
        
        #================================================================#
        print("==========Smooth the Signal for G channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(g_mean) # Here the G Channel For Smoothing
        plot_time_series(smooth_signal, "g", "Smoth PPG signal for G channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform G: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "g", "extract_ppg_single_waveform_with_smooth for G")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_G.append(extract_ppg45_feat)
        print(len(table_PPG_Smooth_G))
        
        #############################
        #    B channel             #            
        #############################
        print("==============For B channel:===================")
        plot_time_series(g_mean, "b", "B Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(b_mean) # Here the B Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform B: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "b", "extract_ppg_single_waveform_without_smooth for B")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_B.append(extract_ppg45_feat)
#        print(len(table_PPG_NonSmooth_B))
        
        #================================================================#
        print("==========Smooth the Signal for B channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(b_mean) # Here the B Channel For Smoothing
        plot_time_series(smooth_signal, "b", "Smoth PPG signal for B channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform B: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "b", "extract_ppg_single_waveform_with_smooth for B")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_B.append(extract_ppg45_feat)
#        print(len(table_PPG_Smooth_B))


headers = ['ID', 'Systolic_peak(x)', 'Diastolic_peak(y)', 'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)', 'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x', 'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)', 'Dicrotic_notch_time(t3)', 'Time_between_systolic_and_diastolic_peaks(∆T)', 'Time_between_half_systolic_peak_points(w)', 'Inflection_point_area_ratio(A2/A1)', 'Systolic_peak_rising_slope(t1/x)', 'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi', 't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2', '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi', 'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi', '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)', 'Fundamental_component_magnitude(|sbase|)', '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)', '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)', 'Stress-induced_vascular_response_index(sVRI)']       

df = pd.DataFrame(table_PPG_NonSmooth_R, columns=headers)
df.to_csv('CSV_Files/LED-0940_PPG_CSV/PPG_NonSmooth_R_0940.csv')
df = pd.DataFrame(table_PPG_Smooth_R, columns=headers)
df.to_csv('CSV_Files/LED-0940_PPG_CSV/PPG_Smooth_R_0940.csv')

df = pd.DataFrame(table_PPG_NonSmooth_G, columns=headers)
df.to_csv('CSV_Files/LED-0940_PPG_CSV/PPG_NonSmooth_G_0940.csv')
df = pd.DataFrame(table_PPG_Smooth_G, columns=headers)
df.to_csv('CSV_Files/LED-0940_PPG_CSV/PPG_Smooth_G_0940.csv')

df = pd.DataFrame(table_PPG_NonSmooth_B, columns=headers)
df.to_csv('CSV_Files/LED-0940_PPG_CSV/PPG_NonSmooth_B_0940.csv')
df = pd.DataFrame(table_PPG_Smooth_B, columns=headers)
df.to_csv('CSV_Files/LED-0940_PPG_CSV/PPG_Smooth_B_0940.csv')


##################################################################################
'''                R Channel , G Channel, B Channel  For LED-1070    '''
##################################################################################

#########
table_PPG_NonSmooth_R = []
table_PPG_NonSmooth_G = []
table_PPG_NonSmooth_B = []

table_PPG_Smooth_R = []
table_PPG_Smooth_G = []
table_PPG_Smooth_B = []

""" Generate .csv files for  R, G, B channels respectively . """

#src_dir = "Extract-Images"
#src_dir = "RawData/Extract-Images"
LED_Broad_num = ["/LED-1070"]
#LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"] #, "/LED-0940", "/LED-1070","/LED-NO"

for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    print("Path : " + str(path))
    
    for folder in folList:
        print("Folder : " + str(folder))
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
#        print(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
            
        ''' # Plot the time series of each channel    
        ### Plot the Red channel time series
        plot_time_series(r_mean, "r", "Red Channel")
        
        ### Plot the Green channel time series
        plot_time_series(g_mean, "g", "Green Channel")
        
        ### Plot the Blue channel time series
        plot_time_series(b_mean, "b", "Blue Channel")
        '''
        
        ##### High Pass filter apply on each channel ####################
        fps = 60
        cutoff = 0.5
        high_pass_filter_R = butter_highpass_filter(r_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_R, 'r', label='High pass filter for R channel')
        
        high_pass_filter_G = butter_highpass_filter(g_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_G, 'g', label='High pass filter for G channel')
        
        high_pass_filter_B = butter_highpass_filter(b_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_B, 'b', label='High pass filter for B channel')
        
        
        ############## Fast FFT on high pass filtered channel ###############
        FFT_R = FFT(high_pass_filter_R)
        #plot_time_series(FFT_R, 'r', label='FFT for R channel')
        
        FFT_G = FFT(high_pass_filter_G)
        #plot_time_series(FFT_G, 'g', label='FFT for G channel')
        
        FFT_B = FFT(high_pass_filter_B)
        #plot_time_series(FFT_B, 'b', label='FFT for B channel')
        
        
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
    
        #############################
        #    R channel             #            
        #############################
        print("==============For R channel:===================")
        plot_time_series(r_mean, "r", "R Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(r_mean) # Here the R Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform R: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "r", "extract_ppg_single_waveform_without_smooth for R")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_R.append(extract_ppg45_feat)
        
        #================================================================#
        print("==========Smooth the Signal for R channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(r_mean) # Here the R Channel For Smoothing
        plot_time_series(smooth_signal, "r", "Smoth PPG signal for R channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform R: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "r", "extract_ppg_single_waveform_with_smooth for R")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_R.append(extract_ppg45_feat)
        
        
        #############################
        #    G channel             #            
        #############################
        print("==============For G channel:===================")
        plot_time_series(g_mean, "g", "G Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(g_mean) # Here the G Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform G: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "g", "extract_ppg_single_waveform_without_smooth for G")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_G.append(extract_ppg45_feat)
        
        #================================================================#
        print("==========Smooth the Signal for G channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(g_mean) # Here the G Channel For Smoothing
        plot_time_series(smooth_signal, "g", "Smoth PPG signal for G channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform G: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "g", "extract_ppg_single_waveform_with_smooth for G")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_G.append(extract_ppg45_feat)
        print(len(table_PPG_Smooth_G))
        
        #############################
        #    B channel             #            
        #############################
        print("==============For B channel:===================")
        plot_time_series(g_mean, "b", "B Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(b_mean) # Here the B Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform B: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "b", "extract_ppg_single_waveform_without_smooth for B")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_B.append(extract_ppg45_feat)
#        print(len(table_PPG_NonSmooth_B))
        
        #================================================================#
        print("==========Smooth the Signal for B channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(b_mean) # Here the B Channel For Smoothing
        plot_time_series(smooth_signal, "b", "Smoth PPG signal for B channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform B: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "b", "extract_ppg_single_waveform_with_smooth for B")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_B.append(extract_ppg45_feat)
#        print(len(table_PPG_Smooth_B))


headers = ['ID', 'Systolic_peak(x)', 'Diastolic_peak(y)', 'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)', 'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x', 'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)', 'Dicrotic_notch_time(t3)', 'Time_between_systolic_and_diastolic_peaks(∆T)', 'Time_between_half_systolic_peak_points(w)', 'Inflection_point_area_ratio(A2/A1)', 'Systolic_peak_rising_slope(t1/x)', 'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi', 't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2', '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi', 'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi', '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)', 'Fundamental_component_magnitude(|sbase|)', '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)', '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)', 'Stress-induced_vascular_response_index(sVRI)']       

df = pd.DataFrame(table_PPG_NonSmooth_R, columns=headers)
df.to_csv('CSV_Files/LED-1070_PPG_CSV/PPG_NonSmooth_R_1070.csv')
df = pd.DataFrame(table_PPG_Smooth_R, columns=headers)
df.to_csv('CSV_Files/LED-1070_PPG_CSV/PPG_Smooth_R_1070.csv')

df = pd.DataFrame(table_PPG_NonSmooth_G, columns=headers)
df.to_csv('CSV_Files/LED-1070_PPG_CSV/PPG_NonSmooth_G_1070.csv')
df = pd.DataFrame(table_PPG_Smooth_G, columns=headers)
df.to_csv('CSV_Files/LED-1070_PPG_CSV/PPG_Smooth_G_1070.csv')

df = pd.DataFrame(table_PPG_NonSmooth_B, columns=headers)
df.to_csv('CSV_Files/LED-1070_PPG_CSV/PPG_NonSmooth_B_1070.csv')
df = pd.DataFrame(table_PPG_Smooth_B, columns=headers)
df.to_csv('CSV_Files/LED-1070_PPG_CSV/PPG_Smooth_B_1070.csv')


##################################################################################
'''                R Channel , G Channel, B Channel  For LED-NO    '''
##################################################################################

#########
table_PPG_NonSmooth_R = []
table_PPG_NonSmooth_G = []
table_PPG_NonSmooth_B = []

table_PPG_Smooth_R = []
table_PPG_Smooth_G = []
table_PPG_Smooth_B = []

""" Generate .csv files for  R, G, B channels respectively . """

#src_dir = "Extract-Images"
#src_dir = "RawData/Extract-Images"
LED_Broad_num = ["/LED-NO"]
#LED_Broad_num = ["/LED-0850", "/LED-0940", "/LED-1070","/LED-NO"] #, "/LED-0940", "/LED-1070","/LED-NO"

for board in LED_Broad_num:
    path = src_dir + board + "/*" 
    folList = glob.glob(path)
#    print(folList)
    print("Path : " + str(path))
    
    for folder in folList:
        print("Folder : " + str(folder))
        file_names = []
        for file in os.listdir(folder):
            file_names.append(file)
            
        file_names = sorted(file_names,  key = lambda x: int(x[:-4]))
        #Files.append(file_names)
#        print(file_names)
        print("###########################################")
        print("Frame numbers: " + str(len(file_names)))
#        print(file_names)
        
        
        r_mean = []
        g_mean = []
        b_mean = []
        
        seq_num = []
        
        
        file_cnt = 0
        
        len_file = len(file_names)
        
        if len_file > 600:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == 600: # Take 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
                
        else:
            for file in file_names:
                img = cv2.imread(os.path.join(folder,file))
                
                #print(file[:-4])
                
                if file_cnt == len_file - 1: # Take < 600 frames.
                    break
                    
                file_cnt += 1
                
                seq_num.append(int(file[:-4]))
                
                average_color = [img[:, :, i].mean() for i in range(img.shape[-1])]
                
                #BGR
                b_mean.append(average_color[0])
                g_mean.append(average_color[1])
                r_mean.append(average_color[2])
            
        ''' # Plot the time series of each channel    
        ### Plot the Red channel time series
        plot_time_series(r_mean, "r", "Red Channel")
        
        ### Plot the Green channel time series
        plot_time_series(g_mean, "g", "Green Channel")
        
        ### Plot the Blue channel time series
        plot_time_series(b_mean, "b", "Blue Channel")
        '''
        
        ##### High Pass filter apply on each channel ####################
        fps = 60
        cutoff = 0.5
        high_pass_filter_R = butter_highpass_filter(r_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_R, 'r', label='High pass filter for R channel')
        
        high_pass_filter_G = butter_highpass_filter(g_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_G, 'g', label='High pass filter for G channel')
        
        high_pass_filter_B = butter_highpass_filter(b_mean, cutoff, fps)
        # plot_time_series(high_pass_filter_B, 'b', label='High pass filter for B channel')
        
        
        ############## Fast FFT on high pass filtered channel ###############
        FFT_R = FFT(high_pass_filter_R)
        #plot_time_series(FFT_R, 'r', label='FFT for R channel')
        
        FFT_G = FFT(high_pass_filter_G)
        #plot_time_series(FFT_G, 'g', label='FFT for G channel')
        
        FFT_B = FFT(high_pass_filter_B)
        #plot_time_series(FFT_B, 'b', label='FFT for B channel')
        
        
        ##############################
        LED_Broad = folder.split('/')[-2].split('-')[1]
        print("LED Broad: " + str(LED_Broad))
        
        ID = folder.split('/')[-1].split('-')[0]
        print("ID: " + str(ID))
    
        #############################
        #    R channel             #            
        #############################
        print("==============For R channel:===================")
        plot_time_series(r_mean, "r", "R Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(r_mean) # Here the R Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform R: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "r", "extract_ppg_single_waveform_without_smooth for R")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_R.append(extract_ppg45_feat)
        
        #================================================================#
        print("==========Smooth the Signal for R channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(r_mean) # Here the R Channel For Smoothing
        plot_time_series(smooth_signal, "r", "Smoth PPG signal for R channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform R: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "r", "extract_ppg_single_waveform_with_smooth for R")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_R.append(extract_ppg45_feat)
        
        
        #############################
        #    G channel             #            
        #############################
        print("==============For G channel:===================")
        plot_time_series(g_mean, "g", "G Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(g_mean) # Here the G Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform G: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "g", "extract_ppg_single_waveform_without_smooth for G")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_G.append(extract_ppg45_feat)
        
        #================================================================#
        print("==========Smooth the Signal for G channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(g_mean) # Here the G Channel For Smoothing
        plot_time_series(smooth_signal, "g", "Smoth PPG signal for G channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform G: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "g", "extract_ppg_single_waveform_with_smooth for G")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_G.append(extract_ppg45_feat)
        print(len(table_PPG_Smooth_G))
        
        #############################
        #    B channel             #            
        #############################
        print("==============For B channel:===================")
        plot_time_series(g_mean, "b", "B Channnel")
        
        '''Extract PPG Single Wave Form'''
        extract_ppg_single_waveform_without_smooth = extract_ppg_single_waveform(b_mean) # Here the B Channel
        extract_ppg_single_waveform_without_smooth = sum(extract_ppg_single_waveform_without_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform B: " + str(extract_ppg_single_waveform_without_smooth))
        plot_time_series(extract_ppg_single_waveform_without_smooth, "b", "extract_ppg_single_waveform_without_smooth for B")


        print("========For Non Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_without_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_without_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Not Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_NonSmooth_B.append(extract_ppg45_feat)
#        print(len(table_PPG_NonSmooth_B))
        
        #================================================================#
        print("==========Smooth the Signal for B channel:==============")
        '''Smooth the Signal'''
        smooth_signal = smooth_ppg_signal(b_mean) # Here the B Channel For Smoothing
        plot_time_series(smooth_signal, "b", "Smoth PPG signal for B channel")
        
        extract_ppg_single_waveform_with_smooth = extract_ppg_single_waveform(smooth_signal)
        extract_ppg_single_waveform_with_smooth = sum(extract_ppg_single_waveform_with_smooth, []) ### Need for 1-D 
#        print("Extract PPG signal for SINGLE Waveform B: " + str(extract_ppg_single_waveform_with_smooth))
        plot_time_series(extract_ppg_single_waveform_with_smooth, "b", "extract_ppg_single_waveform_with_smooth for B")
        

        print("========For Smooth Single Wave Form==========")
        '''PPG-45 (39 time-domain, 6 frequency-domain)'''
        extract_ppg45_feat = extract_ppg45(extract_ppg_single_waveform_with_smooth)
#        print("PPG-45 (39 time-domain, 6 frequency-domain): " + str(extract_ppg45_feat))
#        print("Length: " + str(len(extract_ppg45_feat)))
        
        '''Stress-induced vascular response index (sVRI'''
        svri = extract_svri(extract_ppg_single_waveform_with_smooth)
#        print("Stress-induced vascular response index: " + str(svri))
        
        print("After Smooting the signal:------ ")
        extract_ppg45_feat.insert(0, int(ID))
        extract_ppg45_feat.append(svri)
        table_PPG_Smooth_B.append(extract_ppg45_feat)
#        print(len(table_PPG_Smooth_B))


headers = ['ID', 'Systolic_peak(x)', 'Diastolic_peak(y)', 'Dicrotic_notch(z)', 'Pulse_interval(tpi)', 'Augmentation_index(y/x)', 'Relative_augmentation_index((x-y)/x)', 'z/x', '(y-z)/x', 'Systolic_peak_time(t1)', 'Diastolic_peak_time(t2)', 'Dicrotic_notch_time(t3)', 'Time_between_systolic_and_diastolic_peaks(∆T)', 'Time_between_half_systolic_peak_points(w)', 'Inflection_point_area_ratio(A2/A1)', 'Systolic_peak_rising_slope(t1/x)', 'Diastolic_peak_falling_slope(y/(tpi-t3))', 't1/tpi', 't2/tpi', 't3/tpi', '∆T/tpi', 'ta1', 'tb1', 'te1', 'tf1', 'b2/a2', 'e2/a2', '(b2+e2)/a2', 'ta2', 'tb2', 'ta1/tpi', 'tb1/tpi', 'te1/tpi', 'tf1/tpi', 'ta2/tpi', 'tb2/tpi', '(ta1+ta2)/tpi', '(tb1+tb2)/tpi', '(te1+t2)/tpi', '(tf1+t3)/tpi', 'Fundamental_component_frequency(fbase)', 'Fundamental_component_magnitude(|sbase|)', '2nd_harmonic_frequency(f2nd)', '2nd_harmonic_magnitude(|s2nd|)', '3rd_harmonic_frequency(f3rd)', '3rd_harmonic_magnitude(|s3rd|)', 'Stress-induced_vascular_response_index(sVRI)']       

df = pd.DataFrame(table_PPG_NonSmooth_R, columns=headers)
df.to_csv('CSV_Files/LED-NO_PPG_CSV/PPG_NonSmooth_R_NO.csv')
df = pd.DataFrame(table_PPG_Smooth_R, columns=headers)
df.to_csv('CSV_Files/LED-NO_PPG_CSV/PPG_Smooth_R_NO.csv')

df = pd.DataFrame(table_PPG_NonSmooth_G, columns=headers)
df.to_csv('CSV_Files/LED-NO_PPG_CSV/PPG_NonSmooth_G_NO.csv')
df = pd.DataFrame(table_PPG_Smooth_G, columns=headers)
df.to_csv('CSV_Files/LED-NO_PPG_CSV/PPG_Smooth_G_NO.csv')

df = pd.DataFrame(table_PPG_NonSmooth_B, columns=headers)
df.to_csv('CSV_Files/LED-NO_PPG_CSV/PPG_NonSmooth_B_NO.csv')
df = pd.DataFrame(table_PPG_Smooth_B, columns=headers)
df.to_csv('CSV_Files/LED-NO_PPG_CSV/PPG_Smooth_B_NO.csv')