#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 31 22:41:13 2018

@author: rezwan
"""

from sklearn.svm import SVR
from sklearn.model_selection import GridSearchCV
from sklearn import metrics

def support_vector_regression(features, labels):
#    parameters = {
#        'C': [1.0, 10.0, 100.0],
#        'kernel': ['linear', 'poly', 'rbf'],
#        'degree': [2,3,4,5]
#    }
#    model = GridSearchCV(SVR(epsilon=0.2), parameters)
    
    model = SVR(kernel = 'rbf', epsilon=0.2, C = 1.0)
#    model.fit(features, labels)
    return model