#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  2 16:49:11 2019

@author: rezwan
"""

###=========== First of all, we will import the needed dependencies 
from keras.callbacks import ModelCheckpoint
from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error 
from matplotlib import pyplot as plt
import seaborn as sb
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
import warnings 
warnings.filterwarnings('ignore')
warnings.filterwarnings('ignore', category=DeprecationWarning)

### ====================== Read dataset and describe with pearson correlation.
df = pd.read_excel("./Normal_dataset.xlsx")
print(df.describe()) ## Descibe the dataset with statistical measurement indices.

## Show the shape of df
print(df.shape)

## How about correlation of dataset?
print(df.corr())

## How about covariance?
print(df.cov())


#### Seed
import random
seed = 42
random.seed(seed)

#### Correlation matrix plotting function
from utils import *  ### Import all userfunction from utils.py
correlation_matrix(df)

### Plot histogram of all features
hist_feats(df)

###=========================== Split dataset as Inputs(X) and target(y)
## convert into matrix
df_mat = df.as_matrix()

### Standarscaler for Scaling the dataset: z = (x - mu) / sigma
scaler = StandardScaler()
df_scaled = scaler.fit_transform(df_mat)

## Store these off for predictions with unseen data
df_means = scaler.mean_
df_stds = scaler.scale_ 

y = df_scaled[:, 1] ## target
X = np.delete(df_scaled, 1, axis = 1) # Inputs: f1, f2, ..., f14

###### =========================================== (1) Normal : Split into train and test ===============
from sklearn.model_selection import train_test_split
## create training and testing vars ( train = 80%, Test = 20%)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42) 

"""
####====== OR, Take manually first 80% for train
train_size = int(0.8 * X.shape[0])
X_train, X_test, y_train, y_test = X[0:train_size], X[train_size:], y[0:train_size], y[train_size:]
"""

####=========== Now call DNN_model for running the deep learning model
model = DNN_model(X_train)
model.summary()

###====== Define a checkpoint callback :
checkpoint_name = 'Weights-{epoch:03d}--{val_loss:.5f}.hdf5' 
checkpoint = ModelCheckpoint(checkpoint_name, monitor='val_loss', verbose = 1, save_best_only = True, mode ='auto')
callbacks_list = [checkpoint]

###==== Train the model
NUM_EPOCHS = 50 
BATCH_SIZE = 32
model.fit(X_train, y_train, epochs=NUM_EPOCHS, batch_size=BATCH_SIZE, validation_split = 0.2, callbacks=callbacks_list)

######===== predict the values
y_test_ = model.predict(X_test).flatten()
for i in range(X_test.shape[0]):
    actual = (y_test[i] * df_stds[1]) + df_means[1]
    y_pred = (y_test_[i] * df_stds[1]) + df_means[1]
    print("Expected: {:.3f}, Pred: {:.3f}".format(actual,y_pred ))
    
actual = (y_test * df_stds[1]) + df_means[1]
y_pred = (y_test_ * df_stds[1]) + df_means[1]

################# ====== plot bland_altman_plot
bland_altman_plot(actual, y_pred)

##### === Plot estimated and predicted
r = pearson_corr(actual, y_pred)
act_pred_plot(actual, y_pred, r)

###====================== Performances of model
from sklearn import metrics
#### Correllation of determination
print("R^2: " + str(metrics.r2_score(actual,y_pred))) 
### Correlation coefficient : R
print("R(CC) :" +str(pearson_corr(actual, y_pred)))
### MSE
print("MSE: " + str(metrics.mean_squared_error(actual, y_pred)))
### MAE
print("MAE: " + str(metrics.mean_absolute_error(actual, y_pred)))
## RMSE
print("RMSE: " +str(rmse(y_pred, actual)))





##########################################################################################################################
#### =================================== (2) Now we'll apply 10-fold cross validation
#########################################################################################################################
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn import metrics
############### Apply 10-fold Cross validation
n_splits = 10 ## Fold number
cv_set = np.repeat(-1.,X.shape[0])
skf = KFold(n_splits = n_splits ,shuffle=True, random_state=42)
for train_index,test_index in skf.split(X, y):
    x_train,x_test = X[train_index],X[test_index]
    y_train,y_test = y[train_index],y[test_index]
    if x_train.shape[0] != y_train.shape[0]:
        raise Exception()

    ### === Model
    model = DNN_model(x_train)
    ###==== Train the model
    NUM_EPOCHS = 50 
    BATCH_SIZE = 32
    model.fit(x_train, y_train, epochs=NUM_EPOCHS, batch_size=BATCH_SIZE, verbose = 1)

    predicted_y = model.predict(x_test).flatten()
    cv_set[test_index] = predicted_y
 

### ===== For Get real values
y_true = (y * df_stds[1]) + df_means[1]
y_pred = (cv_set * df_stds[1]) + df_means[1]
print("Over R^2: " + str(metrics.r2_score(y_true, y_pred)))

################# ====== plot bland_altman_plot
bland_altman_plot(y_true, y_pred)

##### === Plot estimated and predicted
r = pearson_corr(y_true, y_pred)
act_pred_plot(y_true, y_pred, r)

###====================== Performances of model
from sklearn import metrics
#### Correllation of determination
print("R^2: " + str(metrics.r2_score(y_true,y_pred))) 
### Correlation coefficient : R
print("R(CC) :" +str(pearson_corr(y_true, y_pred)))
### MSE
print("MSE: " + str(metrics.mean_squared_error(y_true, y_pred)))
### MAE
print("MAE: " + str(metrics.mean_absolute_error(y_true, y_pred)))
## RMSE
print("RMSE: " +str(rmse(y_pred, y_true)))
