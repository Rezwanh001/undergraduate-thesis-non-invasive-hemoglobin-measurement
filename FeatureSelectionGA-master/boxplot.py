#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 26 17:24:37 2019

@author: rezwan
"""

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns, matplotlib.pyplot as plt, operator as op
sns.set(style="whitegrid")


plot_data = {
  '1-Daylatter': [0.02339976, 0.03235323, 0.12835462, 0.10238375, 0.04223188],
    '3-Daylatter': [0.02339976, 0.03235323, 0.12835462, 0.10238375, 0.04223188],
    '6-monthlatter': [0.02339976, 0.03235323, 0.12835462, 0.10238375, 0.04223188]
}

# sort keys and values together
sorted_keys, sorted_vals = zip(*sorted(plot_data.items(), key=op.itemgetter(1)))

# almost verbatim from question
sns.set(context='notebook', style='whitegrid')
sns.utils.axlabel(xlabel="", ylabel="Differences", fontsize=16)
sns.boxplot(data=sorted_vals, width=.18)
sns.swarmplot(data=sorted_vals, size=6, edgecolor="black", linewidth=.9)

# category labels
plt.xticks(plt.xticks()[0], sorted_keys)

plt.show()