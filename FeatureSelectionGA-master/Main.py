#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 19:44:39 2018

@author: rezwan
"""

## import necessary libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

## Read Input files
df = pd.read_excel("./Normal_dataset.xlsx")

## Show first 5 subjects
print(df.head())

## Show the shape of df
print(df.shape)

## convert into matrix
df = df.as_matrix()

### split Input feature and Labels
y = df[:,[1]] ## label
X = df[:,[0,2,3,4,5,6,7,8,9,10,11,12,13,14]]  ## input feature 

## split the dataset with train and test set
##========== If you want to take first 80% values into train set then follow (1) otherwise follow (2) 
############ random selection
#####(1) 
train_size = int(0.8 * X.shape[0])
X_train, X_test, y_train, y_test = X[0:train_size], X[train_size:], y[0:train_size], y[train_size:]
    
####(2)
#from sklearn.model_selection import train_test_split
#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

### Now, You are ready for applying GA on your datset for feature selection.

### import GA's files
from feature_selection_ga import *
from fitness_function import *

#### Seed
import random
seed = 42
random.seed(seed)

### Now run
#from sklearn.svm import SVR
#model = SVR()
fsga = Feature_Selection_GA(X_train,y_train)
pop = fsga.generate(100, 50) ## population size and Generation = 10,50
pp = fsga.plot_feature_set_score(50) ## Generation

'''
#####================ OMG. It is better than prvious. R = 0.790981076280349
Best individual is [1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0], (0.790981076280349,)
###### =========================== ###
'''

"""
### Try and Check it Manually. :) 


#y = [2,3,4,4]
#X = [[1,2,3],
#     [6,3,4],
#     [4,2,3],
#     [7,5,3]]
#
#print("##################")
#print(len(X))
#print(len(y))
#print("##################")

row = X.shape[0]
col = X.shape[1]
feat = col

DX = []
DY = []

import math
import numpy as np
for i in range(row):
  for j in range(col):
    Dy = (y[i] - y[j])
    sm = 0
    if Dy >= 0:
      sm = sm
      for k in range(feat):
        # print(i, j, k)
        sm += (X[i][k] - X[j][k])**2

    elif Dy < 0:
      sm = -sm
      for k in range(feat):
        # print(i, j, k)
        sm += (X[i][k] - X[j][k])**2
    
    ## Sum
    DY.append(Dy)
    DX.append(math.sqrt(sm / feat))
      
print(len(DY))
print(len(DX))
print("=============")
print(DX)
print(DY)

###================ Calculate SDxDy
DX_mean = np.mean(DX)
#print(DX_mean)
DY_mean = np.mean(DY)
#print(DY_mean)

sm_DX_DY = 0
for i in range(len(DX)):
    sm_DX_DY += (DX[i] - DX_mean) * (DY[i] - DY_mean)

SDxDy = sm_DX_DY / (feat-1)
print("SDxDy : " + str(SDxDy))
    
###===============Calculte SDx
sm_DX = 0
for i in range(len(DX)):
    sm_DX += (DX[i] - DX_mean)**2
    
SDx = sm_DX / (feat-1)
print("SDx : " + str(SDx))

###========== Calculte SDy
sm_Dy = 0
for i in range(len(y)):
    sm_Dy += (DY[i] - DY_mean)**2

SDy = sm_Dy / (feat - 1)
print("SDy : " + str(SDy))    

#### Now Calculate corelation-Coefficient: R
R = (SDxDy) / math.sqrt(SDx * SDy)
print("R : " +str(R)) 
"""