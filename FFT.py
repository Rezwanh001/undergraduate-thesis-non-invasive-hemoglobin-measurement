#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 12 20:54:51 2018

@author: rezwan

# https://github.com/balzer82/FFT-Python/blob/master/FFT-Tutorial.ipynb
"""

from libraries import *

def FFT(filtered):
    fast_FFT = np.fft.fft(filtered)
    
    return fast_FFT